#!/bin/bash

cd modules/diagnostic && make
cd ../query && make
cd ../..
zip -j plc-query.zip -@ < distribution.lst