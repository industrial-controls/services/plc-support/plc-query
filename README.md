# PLC Query

A PLC Query agent composed of a Python 3 executor module and a native 32-bit C++ query utility (used for legacy protocol S7 and Modbus)