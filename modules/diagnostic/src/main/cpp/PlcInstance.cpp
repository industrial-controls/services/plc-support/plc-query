/*
 * PlcInstance.cpp
 *
 *  Created on: Aug 6, 2013
 *      Author: enice
 */

#include "PlcInstance.h"

#include "PlcLogger.h"

using namespace log4cplus;

PlcInstance::PlcInstance() {
	reachable = 0;
}

PlcInstance::~PlcInstance() {
	if(this->driver && this->driver->isConnected()){
		this->driver->disconnect();
	}
}

Driver* PlcInstance::getDriver(){
	return this->driver;
}

void PlcInstance::setDriver(Driver* driver){
	this->driver = driver;
}

string PlcInstance::getIpAddress()
{
	return this->IP;
}

void PlcInstance::setIpAddress(string ipAddress)
{
	this->IP = ipAddress;
}
int PlcInstance::connect (){
	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(this),LOG4CPLUS_TEXT("Connecting PLC "<< getLogicalName()));
	int result = this->driver->connect();
	if(result == 1)
	{
	  LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(this),LOG4CPLUS_TEXT("PLC "<< getLogicalName()<<" now connected"));
	}
	return result;
}

int PlcInstance::disconnect(){
	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(this),LOG4CPLUS_TEXT("Disconnecting PLC "<< getLogicalName()));
	this->driver->disconnect();
	return 1;

}

bool PlcInstance::isConnected(){
	return this->driver->isConnected();
}

string  PlcInstance::getHostname(){
	return this->hostname;
}
void PlcInstance::setHostName(string  hostName){
	this->hostname = hostname;
}

string  		PlcInstance::getCategory(){
	return this->category;
}

void		PlcInstance::setCategory(string  category){
	this->category = category;
}

string  		PlcInstance::getLogicalName(){
	return this->logicalName;
}

void		PlcInstance::setLogicalName(string  logicalName){
	this->logicalName = logicalName;
}


struct PlcInfoUnionList* PlcInstance::queryInfo(){
	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(this),LOG4CPLUS_TEXT("Querying Info for PLC "<< getLogicalName()));
	return this->driver->queryPlcInfo();
}

