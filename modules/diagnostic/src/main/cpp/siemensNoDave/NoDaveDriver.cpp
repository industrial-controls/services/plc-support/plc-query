/*
 * NoDaveDriver.cpp
 *
 *  Created on: Sep 16, 2014
 *      Author: enice
 */

#include "siemensNoDave/NoDaveDriver.h"

#include "siemensNoDave/libS7.h"

#include "PlcLogger.h"

#define FN_S7EVENTS 	"./ListIDEvent.txt"
#define FN_ITEMNAMES 	"./ListIDLibS7.txt"
#define S7_BUFSIZE		16384


NoDaveDriver::NoDaveDriver(string ip_addr, string plc_name) : Driver()
{

	this->IP = ip_addr;
	pConnexion = (s7Connexion *)NULL;

}

NoDaveDriver::~NoDaveDriver()
{
	disconnect();
}

int NoDaveDriver::connect()
{

	if (this->IP == "") {

		if (this->IP == "") {
			// still not found
			this->reachable = -1;
			return this->reachable;
		}
	}
	// hostname could be resolved

	// Params: Connection, IP, Slot, secTimeout, S7type
	char ip[16];
	int code = 0;
	strncpy(ip, IP.c_str(), sizeof(ip));

	code = connectNoDave(ip);
	if ( code < 0 ) {
			// IP address not reachable
			this->reachable = -1;
			pConnexion = (s7Connexion *)NULL;
		}
		else {
			// No problems

			this->reachable = 0;

			// Find out the S7 type and the slot

			s7Connexion *temp = (s7Connexion *)pConnexion;
			if (  temp->iTypePLC == SIEMENS_S300 )
				S7type = 300;
			else if ( temp->iTypePLC == SIEMENS_S400 )
				S7type = 400;

			slot = temp->pConnexion->slot;

			//cout << "connected : slot " << slot << endl;
			this->reachable = 1;


		}

	//cout << "connectNoDave(ip) returns " << code << endl;
	//cout << "Reachable: " << this->reachable << endl;
	return this->reachable;
}

int NoDaveDriver::disconnect()
{
	if(pConnexion != (s7Connexion *)NULL){

		int result = s7Disconnect( (s7Connexion *)pConnexion);
		if(result == 0){
			free((s7Connexion *) pConnexion);
			pConnexion = (s7Connexion *)NULL;
			this->reachable=result;
		}else{
			cout << "ERROR  " << result << " : unable to disconnect" << endl;
		}

		return result;
	}else{
		return -1;
	}
}


int NoDaveDriver::connectNoDave(string ipAddress)
{
		char ip[16];
		strncpy(ip, ipAddress.c_str(), sizeof(ip));
		return s7connect((s7Connexion**)(&pConnexion), ip, 0, 3);
}

void NoDaveDriver::createInfo()
{
	ostringstream bufInfo;
	ostringstream bufDiag;


	bufInfo << "Siemens PLC S7 ";
	if (S7type == 0)
		bufInfo << "[unknown type]";
	else
		bufInfo << S7type;
	bufInfo << "; CPU slot: ";

	if (slot == 0)
		bufInfo << "[unknown]";
	else
		bufInfo << slot;
	bufInfo << ".\n\n";

	bufInfo << sReference;
	bufInfo << sHardware;
	bufInfo << sFirmware << "\n";

	bufDiag << sPlcTime << "\n";
	bufInfo << sPlcCycleTime << "\n";
	bufInfo << sPlcLastStartDate;
	bufDiag << plcDiagEvent;

	info = bufInfo.str();
	diag = bufDiag.str();
}

string NoDaveDriver::convertToString(int number){
	std::stringstream ss;

	ss << number;

	return ss.str();
}

bool NoDaveDriver::isConnected(){
	if(this->reachable == 1){
		return true;
	}else
		return false;
}

/**
 * Read diagnostic data (SZL) from PLC
 */
struct PlcInfoUnionList* NoDaveDriver::queryPlcInfo()
{

	resetData();

	PlcInfoUnionList* result = initPlcInfoUnionList(20);

	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromDriverDetails(this), "Querying led status");

	if (pConnexion == (s7Connexion *)NULL) {
		// still no success: return.
		resetData();
		// return only one test
		return result;
	}

	string value;

	uc pData1[1024];
	// Read status of the LEDs without libS7
	//if (0 == daveReadSZL(((s7Connexion *)pConnexion)->pConnexion, 0x0019, 0x0000, pData1, 100)) {
	if (0 == daveReadSZL(((s7Connexion *)pConnexion)->pConnexion, 0x0019, 0x0000, pData1, 100)) {

		uc numberLeds = 0x100 * pData1[6] + pData1[7];
		int intf = 0, ext = 0, sf = 0, bus1 = 0, bus2 = 0;

		for (uc l = 0; l < numberLeds; l ++) {
			uc idLed = pData1[l * 4 + 9];
			// invert status, because value=1 in the frame means led is lit
			// (-> bad, except for LED RUN), but diamon detail value 1 means
			// ok/green
			int ledStatus = (int) pData1[l * 4 + 10]; // 1 -> ERR
			int iStatus = 0;


			if (ledStatus == 0x00)
				iStatus = 0;
			else
				iStatus = 1;

			switch (idLed) {

			case 0x01: // SF
				addIntPropertyToList(result, "SI_SF", iStatus);
				sf = iStatus;
				break;
			case 0x02: // INTF
				addIntPropertyToList(result, "SI_INTF", iStatus);
				intf = iStatus;
				break;
			case 0x03: // EXTF
				addIntPropertyToList(result, "SI_EXT", iStatus);
				ext = iStatus;
				break;
			case 0x04: // RUN
				addIntPropertyToList(result, "SI_MODE_RUN", iStatus);
				break;
			case 0x0b: // BUS1F
				addIntPropertyToList(result, "SI_BUS_1", iStatus);
				bus1 = iStatus;
				break;
			case 0x0c: // BUS2F
				addIntPropertyToList(result, "SI_BUS_2", iStatus);
				bus2 = iStatus;

				break;
			default:
				break;
			} // switch (idLed)

		}

		if(S7type == 400){
			if((intf==1 || ext==1)){
				addIntPropertyToList(result, "SI_SF", 1);
			}else{
				addIntPropertyToList(result, "SI_SF", 0);
			}
		}

		if(bus1==1 || bus2==1){
			addIntPropertyToList(result, "SI_BUSF", 1);
		}else{
			addIntPropertyToList(result, "SI_BUSF", 0);
		}
	}
	else {
		// error --> trigger a reconnect
		//cout << "Disconnect due to daveReadSZL" << endl;
		//disconnect();
		//cout << "###### No data from daveReadSZL" << endl;

//		int result = s7Disconnect( (s7Connexion *)pConnexion);
//		if(result == 0){
//			this->reachable = 0;
//			free((s7Connexion *) pConnexion);
//			pConnexion = (s7Connexion *)NULL;
//		}else{
//			cout << "ERROR  " << result << " : unable to disconnect" << endl;
//		}

		// Sometimes there is a SIGPIPE thrown
		// But in this case, it's not the PLC that resets the connection,
		// but we have a network error

		return result;
	}


	//call the s7 diagnostic function

	uc iIndexBuffer, bFlagEnd ;
	THeader* pHeader;
	TDateAndTime* pDTime;
	unsigned short int* pData;
	unsigned short int iTypeData;
	// don't read status of leds
	unsigned short int bitsidrequest = PLC_ALL ^ PLCRunMode ^ PLCIOErr ^ PLCErr;
	if (0==s7GetDiagnostic((s7Connexion *)pConnexion, bitsidrequest, pData1, 1024)){

		reachable = 1;

		iIndexBuffer = 0;									// for read the beginning of the buffer
		bFlagEnd = 0; 										// not the last header
		do{													// for each data item

			pHeader = (THeader*) &pData1[iIndexBuffer];		// get a header

			if (pHeader->iID == 0x0000){					// look for last buffer
				bFlagEnd = 1;
			}
			else {
				int sizeItem = 0;

				char *itemName;
				string sItemName;
				itemName = s7GetTextID(pHeader->iID, FN_ITEMNAMES, &sizeItem);

				if(itemName){
					sItemName = itemName;
				}
				if (itemName) {
					free(itemName);
					itemName = 0;
				}

				iTypeData = pHeader->iTypeData;
				iIndexBuffer+= sizeof(THeader);

				if (0x0102==iTypeData){
					// Cycle time
					pData = (unsigned short int*) &pData1[iIndexBuffer];// for value on an unsigned char

					ostringstream cycletime;
					cycletime << sItemName << " : " << *pData << "\n";
					sPlcCycleTime = cycletime.str();
				}
				if (0x0114==iTypeData){
					// Module identification
					// reads the characters till the '\0' sign
					sReference = (char*) &pData1[iIndexBuffer];
					sReference = "Module ID: " + sReference + "\n";
				}
				if (0x0104==iTypeData){
					// Hardware OR Firmware version
					if (pHeader->iID == 0x0502) {
						// Hardware
						// only the 2nd Byte is interesting
						char hw[10];
						sprintf(hw, "%d", (char) pData1[iIndexBuffer + 1]);
						sHardware = hw[0];
						sHardware = "Hardware ID: " + sHardware + "\n";
					}
					else if (pHeader->iID == 0x0503) {
						// Firmware
						char fw[7];
						sprintf(fw, "%c %d.%d.%d", (char) pData1[iIndexBuffer],
								(char) pData1[iIndexBuffer+1],
								(char) pData1[iIndexBuffer+2],
								(char) pData1[iIndexBuffer+3]);
						sFirmware = fw;
						sFirmware = "Firmware ID: " + sFirmware + "\n";
					}
				}

				if (0x0208==iTypeData){
					// PLC time
					pDTime = (TDateAndTime*) &pData1[iIndexBuffer];	// for date and time

					char buf[63];
					sprintf (buf, "%02d/%02d/%02d - %02d:%02d:%02d;%03d",pDTime->iDay,
												pDTime->iMonth,
												pDTime->iYear,
												pDTime->iHour,
												pDTime->iMinute,
												pDTime->iSecond,
												pDTime->iMillisecond);
					string timestamp(buf);
					sPlcTime = timestamp;
					addStringPropertyToList(result, "SI_TIME", sPlcTime.c_str());
				}
				if (0x030A==iTypeData){
					// Diagnostic event or PLCLastStartDate
					pDTime = (TDateAndTime*) &pData1[iIndexBuffer];

					char buf[63];
					sprintf (buf, "%02d/%02d/%02d - %02d:%02d:%02d;%03d",pDTime->iDay,
												pDTime->iMonth,
												pDTime->iYear,
												pDTime->iHour,
												pDTime->iMinute,
												pDTime->iSecond,
												pDTime->iMillisecond);
					string timestamp(buf);
					pData = (unsigned short int*) &pData1[iIndexBuffer+8];

					string sbuf;
					char* desc = s7GetTextID (*pData, FN_S7EVENTS, &sizeItem);


					if(desc){
						sbuf = desc;

					}


					if (desc != 0) {
						free(desc);
						desc = 0;
					}
					sbuf = sItemName + " : " + timestamp + "\n" + sbuf + "\n";


					if (sItemName == "PLCLastStartDate") {
						sPlcLastStartDate = sbuf;
					}
					else {
						// Diagnostic event
						plcDiagEvent += sbuf + "\n";
					}
				}
				iIndexBuffer += (iTypeData % 0x0100); // go to read the next header //
			}
		}
		while(bFlagEnd == 0);



		createInfo();
		addStringPropertyToList(result, "SI_INFO", this->info);
		addStringPropertyToList(result, "SI_DIAG", this->diag);
//				addStringPropertyToList(result, "SI_INFO", "N/A");
//				addStringPropertyToList(result, "SI_DIAG", "N/A");

	} // if 0 == s7GetDiagnostic
	else {
		//cout << "Disconnect due to s7GetDiagnostic" << endl;
	//	disconnect();
		//cout << "###### No data from s7GetDiagnostic" << endl;
//		int result = s7Disconnect( (s7Connexion *)pConnexion);
//		if(result == 0){
//			reachable = 0;
//			free((s7Connexion *) pConnexion);
//			pConnexion = 0;
//		}else{
//			cout << "ERROR  " << result << " : unable to disconnect" << endl;
//		}

		// Sometimes there is a SIGPIPE thrown
		// But in this case, it's not the PLC that resets the connection,
		// but we have a network error
	}

	return result;
}

