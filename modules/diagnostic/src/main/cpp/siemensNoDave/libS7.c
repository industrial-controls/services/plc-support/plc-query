
/*===============================================================================
|	LibS7 : Librairie de diagnostic pour les PLC Siemens						|
|===============================================================================|
| Cette librairie permet la communication avec les PLC Siemens pour en faire	|
|un diagnostic.																	|
| Les fonctions qui la compose utilisent le protocole S7 proprietaire de 		|
|Siemens via la librairie Libnodave.											|
| Liste des fonctions :															|
|	*s7Connect : permet de se connecter � l'automate							|
|	*s7Disconnect : permet de se d�connecter de l'automate						|
|	*s7RunPLC : met l'automate en mode RUN										|
|	*s7StopPLC : met l'automate en mode STOP									|
===============================================================================*/
#include <string.h>
#include "siemensNoDave/libS7.h"

#include <errno.h>



typedef struct {
	uc iNoLED;
	uc* strNomLED;
} TEtatDEL;

unsigned char HexToDec (unsigned char iHexValue){
	return (((iHexValue / 16)*10)+(iHexValue % 16));
}
	
/*===============================================================================
|	s7Connect : Connexion � l'automate											|
|===============================================================================|
|PE : adresse IP, Timeout, Slot(facultatif)										|
|PS : structure de type s7Connexion												|
|VR : Code d'erreur																|
| Le slot d'emphichage pouvant varier pour les S7-400, il peut etre precise en	|
|parametre d'entree. Sinon une tentative de connexion se fait sucessivement	sur	|
|les slots 2, 3 et 4.															|
===============================================================================*/
int s7connect (s7Connexion **pps7Connexion,const char *strIP, int iSlotCPU, long iSecondTimeOut){
	s7Connexion *ps7Connexion;
	daveInterface * dInterface;
	daveConnection * dConnexion;
	_daveOSserialType dIOSockets;
	int iSlot,iResult,iErreur;
	uc pBuffer[36];
	dConnexion = (daveConnection*)NULL;
	dInterface = (daveInterface*)NULL;
	ps7Connexion = malloc (sizeof (s7Connexion));
	ps7Connexion->pConnexion = (daveConnection*)NULL;
	ps7Connexion->iTypePLC = 0;
	if (0==iSlotCPU){
		iSlot =1;
	}
	else{
		iSlot = (iSlotCPU-1);
	}
	do{
		iSlot++;
		#ifdef S7_DEBUG
		printf("Connexion (Slot%d) :  ",iSlot);
		#endif
		dIOSockets.rfd=openSocket(102, strIP);
		#ifdef S7_DEBUG
		printf("Socket opened : %d\n", dIOSockets.rfd);
		#endif
		dIOSockets.wfd=dIOSockets.rfd;
		iErreur = -1;
		if (dIOSockets.rfd<=0) { 
			#ifdef S7_DEBUG
			printf("[ ECHEC ]1\n");
			#endif
			//Erreur lors de la creation de la socket
			iErreur = S7_SYS_ERROR;
		}
		else{
			dInterface =daveNewInterface(dIOSockets,"IF1",0, daveProtoISOTCP, 0);
			daveSetTimeout(dInterface,10000);
			dConnexion =daveNewConnection(dInterface,0,0, iSlot);  /* insert your rack and slot here */
			iResult=daveConnectPLC(dConnexion);
			if (iResult!=0){
				#ifdef S7_DEBUG
				printf("[ ECHEC ]2\n");
				#endif
				//Erreur lors de la connexion
				iErreur = S7_CONNECT_ERROR;		
			} 
			else {
				ps7Connexion->pConnexion = dConnexion;
				#ifdef S7_DEBUG
				printf("[  OK   ]\n");
				#endif
				//Pas d'erreur
				iErreur = 0;		
			}
		}
	// Slot can be 5 as maximum (if there are 2 power supplies)
	}while((-3==iErreur)&&(iSlot<6)&&(iSlotCPU==0));
	//Recherche du type d'automate
	ps7Connexion->iTypePLC = SIEMENS_S300;  //Default
	if (iSlot>-2) ps7Connexion->iTypePLC = SIEMENS_S400;
	if (0==iErreur){
		#ifdef S7_DEBUG
		printf("Lecture Type PLC  :  ");
		#endif
		if (0!=daveReadSZL(dConnexion,0x0011,0x0001,pBuffer,36)){
			#ifdef S7_DEBUG
			printf("[ ECHEC ]\n");
			#endif
		} 
		else {
			switch (pBuffer[15]){
				case '3':
					#ifdef S7_DEBUG
					printf("[ S300  ]\n");
					#endif	
					ps7Connexion->iTypePLC = SIEMENS_S300;
					break;
				case '4':
					#ifdef S7_DEBUG
					printf("[ S400  ]\n");
					#endif	
					ps7Connexion->iTypePLC = SIEMENS_S400;
					break;
				default :
					#ifdef S7_DEBUG
					printf("[ Echec ]\n");
					#endif	
					iErreur = S7_CONNECT_ERROR;
					break;
			}
		}
	}	
	
	//Gestion des erreurs
	if (0==iErreur){
		*pps7Connexion = ps7Connexion;
	}
	else{
		// If the socket ID is valid, close it
		if(dIOSockets.rfd > 0) {
			int err = closeSocket(dIOSockets.rfd);
#ifdef S7_DEBUG
			printf("Socket closed : %d (error code %d, errno %d)",dIOSockets.rfd, err, errno);
#endif
		}

		*pps7Connexion = (s7Connexion*)NULL;
		if (dConnexion == (daveConnection*)NULL){
			free(dConnexion);
		}
		if (dInterface == (daveInterface*)NULL){
			free(dInterface);
		}
		free(ps7Connexion);
	}
	//printf("Erreur %d \n", iErreur);
	return iErreur;
}
/*===============================================================================
|	s7Disconnect : Deconnexion de l'automate									|
|===============================================================================|
|PE : structure de type s7Connexion												|
|VR : Code d'erreur																|
| Utilisation de la fonction de deconnection de Libnodave et fermeture du		|
|socket de communication														|
===============================================================================*/
int s7Disconnect (s7Connexion* ps7Connexion){
	if (ps7Connexion==(s7Connexion*)NULL){
		return S7_PARAM_ERROR;
	}
	else{
		int code=0;
		int close=0;
		int iErreur=-1;
		daveInterface * dInterface;
		dInterface = ps7Connexion->pConnexion->iface;
		#ifdef S7_DEBUG
		printf("Deconnexion :        ");
		#endif
		if (0!=daveDisconnectPLC(ps7Connexion->pConnexion)){
			#ifdef S7_DEBUG
			printf("[ ECHEC ]\n");
			#endif
			//Erreur lors de la deconnexion
			iErreur = S7_DISCONNECT_ERROR;				
		} 
		else {
			#ifdef S7_DEBUG
			printf("[  OK   ]\n");
			#endif
			//Pas d'erreur
			iErreur = 0;				
		}
		code = daveDisconnectAdapter(dInterface);
		if(dInterface->fd.rfd > 0){
		  int err = closeSocket(dInterface->fd.rfd);
#ifdef S7_DEBUG
		  printf("Socket closed : %d (err : %d, errno %d)",dInterface->fd.rfd, err, errno);
#endif
		}
		//free (dInterface);
		//free (ps7Connexion->pConnexion);
		//if (ps7Connexion != 0)
		//	free (ps7Connexion);
//		printf("------------------------------------------\n");
//		printf("Socket disconnection info\nRFD: %d\n", dInterface->fd.rfd);
//		printf("daveDisconnectAdapter: %d\n", code);
//		printf("closeSocket: %d\n", close);
//		printf("iErreur %d\n", iErreur);
//		printf("------------------------------------------\n");
		return iErreur;
	}
}
/*===============================================================================
|	s7RunPLC : Met l'automate en mode RUN										|
|===============================================================================|
|PE : structure de type s7Connexion												|
|VR : Code d'erreur																|
| Utilisation de la fonction "daveStart" de Libnodave							|
===============================================================================*/
int s7RunPLC (s7Connexion* ps7Connexion){
	if (ps7Connexion==(s7Connexion*)NULL){
		return S7_PARAM_ERROR;
	}
	else{
		daveStart(ps7Connexion->pConnexion);
		return 0;
	}
}
/*===============================================================================
|	s7StopPLC : Met l'automate en mode STOP										|
|===============================================================================|
|PE : structure de type s7Connexion												|
|VR : Code d'erreur																|
| Utilisation de la fonction "daveStop" de Libnodave							|
===============================================================================*/
int s7StopPLC (s7Connexion* ps7Connexion){
	if (ps7Connexion==(s7Connexion*)NULL){
		return S7_PARAM_ERROR;
	}
	else{
		daveStop(ps7Connexion->pConnexion);
		return 0;
	}
}
/*===============================================================================
|	s7Read : Fait une lecture sur le PLC										|
|===============================================================================|
|PE : structure de type s7Connexion												|
|	  Type de bloc																|
|	  Numero pour bloc DB														|
|	  Numero du premier octet � lire											|
|	  Nombre d'octet � lire														|
|PS : Pointeur sur les donn�es lues												|
|VR : Code d'erreur																|
| Utilisation de la fonction "daveReadBytes" de Libnodave						|
===============================================================================*/
int s7Read (s7Connexion* ps7Connexion, unsigned char orgId, unsigned char dbNumber,unsigned short start,
											unsigned short size, unsigned char* pData, int iSecondTimeout){
	int iTailleBloc, iTailleDernierbloc, iNbBlocsEntiers, iIndex, iErreur, iResult;
	if (ps7Connexion==(s7Connexion*)NULL){
		iErreur = S7_PARAM_ERROR;
	}
	else{
		switch (ps7Connexion->iTypePLC){
			case SIEMENS_S300 :
				iTailleBloc = 222;
				//printf ("Debug = S300\n");
				break;
			case SIEMENS_S400 :
				iTailleBloc = 462;
				//printf ("Debug = S400\n");
				break;
			default :
				iErreur = S7_PARAM_ERROR;
				break;
		}
		iTailleDernierbloc = size % iTailleBloc;
		iNbBlocsEntiers = size / iTailleBloc;	
		iIndex=0;
		iResult=0; 
		while ((iIndex<iNbBlocsEntiers)&&(0==iResult)){
			//printf ("Debug = Boucle\n");
			iResult= daveReadBytes(ps7Connexion->pConnexion,orgId,dbNumber,start+(iTailleBloc*iIndex),iTailleBloc,&pData[iTailleBloc*iIndex]);
			iIndex++;
		}
		if (iResult >0) iErreur =S7_PARAM_ERROR;
		if (0==iErreur){
			//printf ("Debug = Fin\n");
			iResult= daveReadBytes(ps7Connexion->pConnexion,orgId,dbNumber,start+(iTailleBloc*iNbBlocsEntiers),iTailleDernierbloc,&pData[iTailleBloc*iNbBlocsEntiers]);
			if (iResult >0) iErreur =S7_PARAM_ERROR;
		}
	}
	return iErreur;
}

/*===============================================================================
|	s7Write : Fait une lecture sur le PLC										|
|===============================================================================|
|PE : structure de type s7Connexion												|
|	  Type de bloc																|
|	  Numero pour bloc DB														|
|	  Numero du premier octet � lire											|
|	  Nombre d'octet � lire														|
|	  Pointeur sur les donn�es � �crire											|
|VR : Code d'erreur																|
| Utilisation de la fonction "daveWriteBytes" de Libnodave						|
===============================================================================*/
int s7Write (s7Connexion* ps7Connexion, unsigned char orgId, unsigned char dbNumber,unsigned short start,
											unsigned short size, unsigned char* pData, int iSecondTimeout){
	int iTailleBloc, iTailleDernierbloc, iNbBlocsEntiers, iIndex, iErreur, iResult;
	if (ps7Connexion==(s7Connexion*)NULL){
		iErreur = S7_PARAM_ERROR;
	}
	else{
		switch (ps7Connexion->iTypePLC){
			case SIEMENS_S300 :
				iTailleBloc = 212;
				break;
			case SIEMENS_S400 :
				iTailleBloc = 452;
				break;
			default :
				iErreur = S7_PARAM_ERROR;
				break;
		}
		iTailleDernierbloc = size % iTailleBloc;
		iNbBlocsEntiers = size / iTailleBloc;	
		iIndex=0;
		iResult=0; 
		while ((iIndex<iNbBlocsEntiers)&&(0==iResult)){
			iResult= daveWriteBytes(ps7Connexion->pConnexion,orgId,dbNumber,start+(iTailleBloc*iIndex),iTailleBloc,&pData[iTailleBloc*iIndex]);
			iIndex++;
		}
		if (iResult >0) iErreur =S7_PARAM_ERROR;
		if (0==iErreur){
			iResult= daveWriteBytes(ps7Connexion->pConnexion,orgId,dbNumber,start+(iTailleBloc*iNbBlocsEntiers),iTailleDernierbloc,&pData[iTailleBloc*iNbBlocsEntiers]);
			if (iResult >0) iErreur =S7_PARAM_ERROR;
		}
	}
	return iErreur;
}

/*===============================================================================
|	s7GetDiagnostic : return some diagnostic data                               |
|===============================================================================|
|IN  : struct s7connexion, character strings according to desired data          |
|		ex : "StatusLED" ...                                                    |
|OUT : buffer which will contain the data                                       |
|RV  : error code                                                               |
| La structure du buffer de sortie reste a voir (?)                             |
===============================================================================*/
int s7GetDiagnostic (const s7Connexion *ps7Connexion, unsigned short int iIDRequest,unsigned char* pBuffer, int iBufferLength){
	char iErreur;
	uc pBufferModule[210];
	THeader* pHeader;
	unsigned char iIndexBuffer = 0;
	iErreur = 0; //No error;
	if (iBufferLength < 200){ // the max length is 191 bytes
		iErreur = S7_PARAM_ERROR;  
	}
	else{
		if (((iIDRequest & PLCRunMode)|(iIDRequest & PLCIOErr)|(iIDRequest & PLCErr))&&(0==iErreur)){
	
			unsigned char pBufferLED[100];
			unsigned char iPLCRunMode, iPLCErr, iPLCIOErr; 
			unsigned char iIndex,iIndex2,iNbLED,iNbLedError = 3,iNbLedIOError = 2;
			unsigned char TabLedError[3],TabLedIOError[2],iLedRun,iLedStop;
			TabLedError[0] = 0x01; 	//SF
			TabLedError[1] = 0x02; 	//INTF
			TabLedError[2] = 0x03; 	//EXTF
			TabLedIOError[0] = 0x0B;//BF1
			TabLedIOError[1] = 0x0C;//BF2
			iLedRun        = 0x04; 	//RUN
			iLedStop       = 0x05; 	//STOP
			//Readind SZL 0x0019 for Led state
			iPLCRunMode = 0;
			iPLCErr = 0;
			iPLCIOErr = 0;
			if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0019,0x0000,pBufferLED,100)){
				iErreur = S7_SYS_ERROR;
			}
			else{
				
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%				
//				//Look for run mode and errors
				iNbLED = 0x100*pBufferLED[6]+pBufferLED[7];
				iIndexBuffer = 0;
				for(iIndex = 0;iIndex <iNbLED; iIndex++){
					if (0x00!=pBufferLED[iIndex*4+10]){		//Led on
						if ((0x00==pBufferLED[iIndex*4+8])&&(iLedRun==pBufferLED[iIndex*4+9])){		//Run mode
							iPLCRunMode = 1;
						}
						else{
							for(iIndex2 = 0;iIndex2 <iNbLedError; iIndex2++){
								if ((0x00==pBufferLED[iIndex*4+8])&&(TabLedError[iIndex2]==pBufferLED[iIndex*4+9])){		//PLC Error
									iPLCErr = 1;
								}
							}
							for(iIndex2 = 0;iIndex2 <iNbLedIOError; iIndex2++){
								if ((0x00==pBufferLED[iIndex*4+8])&&(TabLedIOError[iIndex2]==pBufferLED[iIndex*4+9])){		//IO Error
									iPLCIOErr = 1;
								}
							}
						}
					}
				}
				//Build the buffer
				if ((iIDRequest & PLCRunMode)&&(0==iErreur)){
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0201; 							// PLCRunMode
					pHeader->iTypeData = 0x0001;
					iIndexBuffer += sizeof(THeader);
					pBuffer[iIndexBuffer]=iPLCRunMode;
					iIndexBuffer +=1;
				}
				
				if ((iIDRequest & PLCIOErr)&&(0==iErreur)){
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0202; 							// PLCIOErr
					pHeader->iTypeData = 0x0001;
					iIndexBuffer += sizeof(THeader);
					pBuffer[iIndexBuffer]=iPLCIOErr;
					iIndexBuffer +=1;
				}
				
				if ((iIDRequest & PLCErr)&&(0==iErreur)){
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0203; 							// PLCErr
					pHeader->iTypeData = 0x0001;
					iIndexBuffer += sizeof(THeader);
					pBuffer[iIndexBuffer]=iPLCErr;
					iIndexBuffer +=1;	
				}
			}
		} // end retrieving PLCRunMode, PLCIOErr, PLCErr

		if ((iIDRequest & PLCTime)&&(0==iErreur)){

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0132 to get date and time			
			TDateAndTime* pDateAndTime;
			if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0132,0x0008,pBufferModule,210)){
				iErreur = S7_SYS_ERROR;
			}
			else{
				pHeader = (THeader*) &pBuffer[iIndexBuffer];
				pHeader->iID = 0x0301; 								// PLCTime
				pHeader->iTypeData = 0x0200 + sizeof(TDateAndTime);
				iIndexBuffer += sizeof(THeader);
				pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
				pDateAndTime->iYear 	= HexToDec (pBufferModule[30]);
				pDateAndTime->iMonth 	= HexToDec (pBufferModule[31]);
				pDateAndTime->iDay		= HexToDec (pBufferModule[32]);
				pDateAndTime->iHour		= HexToDec (pBufferModule[33]);
				pDateAndTime->iMinute	= HexToDec (pBufferModule[34]);
				pDateAndTime->iSecond	= HexToDec (pBufferModule[35]);
				pDateAndTime->iMillisecond = HexToDec (pBufferModule[36])*10+(pBufferModule[37]/16);
			}
			iIndexBuffer += sizeof(TDateAndTime);
		}
		if ((iIDRequest & PLCCycleTime)&&(0==iErreur)){

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0222 to get the maximum cycle time
			unsigned short int* pData;
			if (0==iErreur){ 
				
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0222,0x0001,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0302; 							// PLCCycleTime
					pHeader->iTypeData = 0x0102;
					iIndexBuffer += sizeof(THeader);
					pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
					*pData = 0x100*pBufferModule[18]+pBufferModule[19];	
				}
			}
			iIndexBuffer += sizeof(unsigned short int);
		}
		
		if ((iIDRequest & PLCModID)&&(0==iErreur)){
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0111 to get module ID
			char* pData;
			if (0==iErreur){
				// read order number/reference
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0111,0x0001,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0501; 					// PLCModuleID
					pHeader->iTypeData = 0x0114;
					iIndexBuffer += sizeof(THeader);

					// pBuffer:outgoing
					// position the pointer on the output
					pData  = (char*) &pBuffer[iIndexBuffer];
					// write value to the positioned pointer
					// value returned by daveReadSZL()
					strncpy(pData, (char*)&pBufferModule[10], 20);
					pData[19] = '\0';
				}
			}
			// output buffer
			iIndexBuffer += 20 * sizeof(char);
		}
		
		if ((iIDRequest & PLCHardware)&&(0==iErreur)){
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0111 to get Hardware version
			char* pData;
			if (0==iErreur){
				// read hardware version
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0111,0x0006,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0502; 					// PLCHardware
					pHeader->iTypeData = 0x0104;
					iIndexBuffer += sizeof(THeader);

					// position the pointer on the output
					pData  = (char*) &pBuffer[iIndexBuffer];
					// write value to the positioned pointer
					memcpy(pData, (char*)&pBufferModule[32], 4);
				}
			}
			// move cursor on the output buffer
			iIndexBuffer += 4 * sizeof(char);
		}
		
		if ((iIDRequest & PLCHardware)&&(0==iErreur)){
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0111 to get Firmware version
			char* pData;
			if (0==iErreur){
				// read hardware version
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0111,0x0007,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0503; 					// PLCFirmware
					pHeader->iTypeData = 0x0104;
					iIndexBuffer += sizeof(THeader);

					// position the pointer on the output
					pData  = (char*) &pBuffer[iIndexBuffer];
					// write value to the positioned pointer
					memcpy(pData, (char*)&pBufferModule[32], 4);
				}
			}
			// move cursor on the output buffer
			iIndexBuffer += 4 * sizeof(char);
		}		
		
		if ((iIDRequest & PLCLastStartDate)&&(0==iErreur)){

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
//			//Read SZL 0x0222 to get LastStartDate
			TDateAndTime* pDateAndTime;
			unsigned short int* pData;
			if (0==iErreur){ 
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0222,0x0064,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0303;  						// PLCLastStartDate
					pHeader->iTypeData = 0x030A;
					iIndexBuffer += sizeof(THeader);
					pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
					pDateAndTime->iYear 	= HexToDec (pBufferModule[20]);
					pDateAndTime->iMonth 	= HexToDec (pBufferModule[21]);
					pDateAndTime->iDay		= HexToDec (pBufferModule[22]);
					pDateAndTime->iHour		= HexToDec (pBufferModule[23]);
					pDateAndTime->iMinute	= HexToDec (pBufferModule[24]);
					pDateAndTime->iSecond	= HexToDec (pBufferModule[25]);
					pDateAndTime->iMillisecond = HexToDec (pBufferModule[26])*10+(pBufferModule[27]/16);
					iIndexBuffer += sizeof(TDateAndTime);
					pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
					*pData = 0x100*pBufferModule[8]+pBufferModule[9];	
					iIndexBuffer += sizeof(unsigned short int);
				}
			}
		}
		if ((iIDRequest & PLCDiag)&&(0==iErreur)){
		
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x00A0 to get Diagnostic Events
			TDateAndTime* pDateAndTime;
			unsigned short int* pData;
			if (0==iErreur){ 
				// XXX
				// read only 10 (0x000A) entries
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x01A0,0x000A,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					unsigned char iNbEvent, iIndex;
					iNbEvent = (0x100*pBufferModule[6]+pBufferModule[7]);
					for(iIndex = 0;iIndex <((iNbEvent>10)?10:iNbEvent); iIndex++){
						pHeader = (THeader*) &pBuffer[iIndexBuffer];
						pHeader->iID = 0x0401  + iIndex; // PLCDiag
						pHeader->iTypeData = 0x030A;
						iIndexBuffer += sizeof(THeader);
						pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
						pDateAndTime->iYear 	= HexToDec (pBufferModule[iIndex*20+20]);
						pDateAndTime->iMonth 	= HexToDec (pBufferModule[iIndex*20+21]);
						pDateAndTime->iDay		= HexToDec (pBufferModule[iIndex*20+22]);
						pDateAndTime->iHour		= HexToDec (pBufferModule[iIndex*20+23]);
						pDateAndTime->iMinute	= HexToDec (pBufferModule[iIndex*20+24]);
						pDateAndTime->iSecond	= HexToDec (pBufferModule[iIndex*20+25]);
						pDateAndTime->iMillisecond = HexToDec (pBufferModule[iIndex*20+26])*10+(pBufferModule[iIndex*20+27]/16);
						iIndexBuffer += sizeof(TDateAndTime);
						pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
						*pData = 0x100*pBufferModule[iIndex*20+8]+pBufferModule[iIndex*20+9];	
						iIndexBuffer += sizeof(unsigned short int);
					}
				}
			}
		}
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Create last header for buffer end

	pHeader = (THeader*) &pBuffer[iIndexBuffer];
	pHeader->iID = 0x0000; // FinBuffer
	pHeader->iTypeData = 0x0000;

	#ifdef S7_DEBUG
	//printf ("\nBuffer length : %d bytes\n",iIndexBuffer+4);
	#endif
	return iErreur;
}
//##########
/*===============================================================================
|	s7GetStatusLed : return status of LED, plc time, cycle time, lastStartDate                             |
|===============================================================================|
|IN  : struct s7connexion, character strings according to desired data          |
|		ex : "StatusLED" ...                                                    |
|OUT : buffer which will contain the data                                       |
|RV  : error code                                                               |
| La structure du buffer de sortie reste a voir (?)                             |
===============================================================================*/
int s7GetStatusLed (const s7Connexion *ps7Connexion, unsigned short int iIDRequest,unsigned char* pBuffer, int iBufferLength){
	char iErreur;
	uc pBufferModule[210];
	THeader* pHeader;
	unsigned char iIndexBuffer = 0;
	iErreur = 0; //No error;
	if (iBufferLength < 200){ // the max length is 191 bytes
		iErreur = S7_PARAM_ERROR;  
	}
	else{
		if (((iIDRequest & PLCRunMode)|(iIDRequest & PLCIOErr)|(iIDRequest & PLCErr))&&(0==iErreur)){
	
			unsigned char pBufferLED[100];
			unsigned char iPLCRunMode, iPLCErr, iPLCIOErr; 
			unsigned char iIndex,iIndex2,iNbLED,iNbLedError = 3,iNbLedIOError = 2;
			unsigned char TabLedError[3],TabLedIOError[2],iLedRun,iLedStop;
			TabLedError[0] = 0x01; 	//SF
			TabLedError[1] = 0x02; 	//INTF
			TabLedError[2] = 0x03; 	//EXTF
			TabLedIOError[0] = 0x0B;//BF1
			TabLedIOError[1] = 0x0C;//BF2
			iLedRun        = 0x04; 	//RUN
			iLedStop       = 0x05; 	//STOP
			//Readind SZL 0x0019 for Led state
			iPLCRunMode = 0;
			iPLCErr = 0;
			iPLCIOErr = 0;
			if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0019,0x0000,pBufferLED,100)){
				iErreur = S7_SYS_ERROR;
			}
			else{
				
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%				
//				//Look for run mode and errors
				iNbLED = 0x100*pBufferLED[6]+pBufferLED[7];
				iIndexBuffer = 0;
				for(iIndex = 0;iIndex <iNbLED; iIndex++){
					if (0x00!=pBufferLED[iIndex*4+10]){		//Led on
						if ((0x00==pBufferLED[iIndex*4+8])&&(iLedRun==pBufferLED[iIndex*4+9])){		//Run mode
							iPLCRunMode = 1;
						}
						else{
							for(iIndex2 = 0;iIndex2 <iNbLedError; iIndex2++){
								if ((0x00==pBufferLED[iIndex*4+8])&&(TabLedError[iIndex2]==pBufferLED[iIndex*4+9])){		//PLC Error
									iPLCErr = 1;
								}
							}
							for(iIndex2 = 0;iIndex2 <iNbLedIOError; iIndex2++){
								if ((0x00==pBufferLED[iIndex*4+8])&&(TabLedIOError[iIndex2]==pBufferLED[iIndex*4+9])){		//IO Error
									iPLCIOErr = 1;
								}
							}
						}
					}
				}
				//Build the buffer
				if ((iIDRequest & PLCRunMode)&&(0==iErreur)){
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0201; 							// PLCRunMode
					pHeader->iTypeData = 0x0001;
					iIndexBuffer += sizeof(THeader);
					pBuffer[iIndexBuffer]=iPLCRunMode;
					iIndexBuffer +=1;
				}
				
				if ((iIDRequest & PLCIOErr)&&(0==iErreur)){
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0202; 							// PLCIOErr
					pHeader->iTypeData = 0x0001;
					iIndexBuffer += sizeof(THeader);
					pBuffer[iIndexBuffer]=iPLCIOErr;
					iIndexBuffer +=1;
				}
				
				if ((iIDRequest & PLCErr)&&(0==iErreur)){
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0203; 							// PLCErr
					pHeader->iTypeData = 0x0001;
					iIndexBuffer += sizeof(THeader);
					pBuffer[iIndexBuffer]=iPLCErr;
					iIndexBuffer +=1;	
				}
			}
		}

		if ((iIDRequest & PLCTime)&&(0==iErreur)){

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0132 to get date and time			
			TDateAndTime* pDateAndTime;
			if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0132,0x0008,pBufferModule,210)){
				iErreur = S7_SYS_ERROR;
			}
			else{
				pHeader = (THeader*) &pBuffer[iIndexBuffer];
				pHeader->iID = 0x0301; 								// PLCTime
				pHeader->iTypeData = 0x0200 + sizeof(TDateAndTime);
				iIndexBuffer += sizeof(THeader);
				pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
				pDateAndTime->iYear 	= HexToDec (pBufferModule[30]);
				pDateAndTime->iMonth 	= HexToDec (pBufferModule[31]);
				pDateAndTime->iDay		= HexToDec (pBufferModule[32]);
				pDateAndTime->iHour		= HexToDec (pBufferModule[33]);
				pDateAndTime->iMinute	= HexToDec (pBufferModule[34]);
				pDateAndTime->iSecond	= HexToDec (pBufferModule[35]);
				pDateAndTime->iMillisecond = HexToDec (pBufferModule[36])*10+(pBufferModule[37]/16);
			}
			iIndexBuffer += sizeof(TDateAndTime);
		}
		if ((iIDRequest & PLCCycleTime)&&(0==iErreur)){

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x0222 to get the maximum cycle time
			unsigned short int* pData;
			if (0==iErreur){ 
				
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0222,0x0001,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0302; 							// PLCCycleTime
					pHeader->iTypeData = 0x0102;
					iIndexBuffer += sizeof(THeader);
					pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
					*pData = 0x100*pBufferModule[18]+pBufferModule[19];	
				}
			}
			iIndexBuffer += sizeof(unsigned short int);
		}
		if ((iIDRequest & PLCLastStartDate)&&(0==iErreur)){

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
//			//Read SZL 0x0222 to get LastStartDate
			TDateAndTime* pDateAndTime;
			unsigned short int* pData;
			if (0==iErreur){ 
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x0222,0x0064,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					pHeader = (THeader*) &pBuffer[iIndexBuffer];
					pHeader->iID = 0x0303;  						// PLCLastStartDate
					pHeader->iTypeData = 0x030A;
					iIndexBuffer += sizeof(THeader);
					pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
					pDateAndTime->iYear 	= HexToDec (pBufferModule[20]);
					pDateAndTime->iMonth 	= HexToDec (pBufferModule[21]);
					pDateAndTime->iDay		= HexToDec (pBufferModule[22]);
					pDateAndTime->iHour		= HexToDec (pBufferModule[23]);
					pDateAndTime->iMinute	= HexToDec (pBufferModule[24]);
					pDateAndTime->iSecond	= HexToDec (pBufferModule[25]);
					pDateAndTime->iMillisecond = HexToDec (pBufferModule[26])*10+(pBufferModule[27]/16);
					iIndexBuffer += sizeof(TDateAndTime);
					pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
					*pData = 0x100*pBufferModule[8]+pBufferModule[9];	
					iIndexBuffer += sizeof(unsigned short int);
				}
			}
		}
/*		if ((iIDRequest & PLCDiag)&&(0==iErreur)){
		
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x00A0 to get Diagnostic Events
			TDateAndTime* pDateAndTime;
			unsigned short int* pData;
			if (0==iErreur){ 
				// XXX
				// read only 10 (0x000A) entries
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x01A0,0x000A,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					unsigned char iNbEvent, iIndex;
					iNbEvent = (0x100*pBufferModule[6]+pBufferModule[7]);
					for(iIndex = 0;iIndex <((iNbEvent>10)?10:iNbEvent); iIndex++){
						pHeader = (THeader*) &pBuffer[iIndexBuffer];
						pHeader->iID = 0x0401  + iIndex; // PLCDiag
						pHeader->iTypeData = 0x030A;
						iIndexBuffer += sizeof(THeader);
						pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
						pDateAndTime->iYear 	= HexToDec (pBufferModule[iIndex*20+20]);
						pDateAndTime->iMonth 	= HexToDec (pBufferModule[iIndex*20+21]);
						pDateAndTime->iDay		= HexToDec (pBufferModule[iIndex*20+22]);
						pDateAndTime->iHour		= HexToDec (pBufferModule[iIndex*20+23]);
						pDateAndTime->iMinute	= HexToDec (pBufferModule[iIndex*20+24]);
						pDateAndTime->iSecond	= HexToDec (pBufferModule[iIndex*20+25]);
						pDateAndTime->iMillisecond = HexToDec (pBufferModule[iIndex*20+26])*10+(pBufferModule[iIndex*20+27]/16);
						iIndexBuffer += sizeof(TDateAndTime);
						pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
						*pData = 0x100*pBufferModule[iIndex*20+8]+pBufferModule[iIndex*20+9];	
						iIndexBuffer += sizeof(unsigned short int);
					}
				}
			}
		}*/
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Create last header for buffer end

	pHeader = (THeader*) &pBuffer[iIndexBuffer];
	pHeader->iID = 0x0000; // FinBuffer
	pHeader->iTypeData = 0x0000;

	#ifdef S7_DEBUG
	//printf ("\nBuffer length : %d bytes\n",iIndexBuffer+4);
	#endif
	return iErreur;
}
//#####

/*===============================================================================
|	s7GetDiagnosticBuffer : return diagnostic events                             |
|===============================================================================|
|IN  : struct s7connexion, character strings according to desired data          |
|		ex : "StatusLED" ...                                                    |
|OUT : buffer which will contain the data                                       |
|RV  : error code                                                               |
| La structure du buffer de sortie reste a voir (?)                             |
===============================================================================*/
int s7GetDiagnosticBuffer (const s7Connexion *ps7Connexion, unsigned short int iIDRequest,unsigned char* pBuffer, int iBufferLength, int numItems){
	char iErreur;
	uc pBufferModule[210];
	THeader* pHeader;
	unsigned char iIndexBuffer = 0;
	iErreur = 0; //No error;
	if (iBufferLength < 200){ // the max length is 191 bytes
		iErreur = S7_PARAM_ERROR;  
	}
	else{
		if (((iIDRequest & PLCRunMode)|(iIDRequest & PLCIOErr)|(iIDRequest & PLCErr))&&(0==iErreur)){
	
			//unsigned char pBufferLED[100];
			//unsigned char iPLCRunMode, iPLCErr, iPLCIOErr; 
			//unsigned char iIndex,iIndex2,iNbLED,iNbLedError = 3,iNbLedIOError = 2;
			//unsigned char TabLedError[3],TabLedIOError[2],iLedRun,iLedStop;
			
		//if ((iIDRequest & PLCDiag)&&(0==iErreur)){
		
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			//Read SZL 0x00A0 to get Diagnostic Events
			TDateAndTime* pDateAndTime;
			unsigned short int* pData;
			if (0==iErreur){ 
				// XXX
				// read only 10 (0x000A) entries
				if (0!=daveReadSZL(ps7Connexion->pConnexion,0x01A0,numItems,pBufferModule,210)){
					iErreur = S7_SYS_ERROR;
				}
				else{
					unsigned char iNbEvent, iIndex;
					iNbEvent = (0x100*pBufferModule[6]+pBufferModule[7]);
					for(iIndex = 0;iIndex <((iNbEvent>10)?10:iNbEvent); iIndex++){
						pHeader = (THeader*) &pBuffer[iIndexBuffer];
						pHeader->iID = 0x0401  + iIndex; // PLCDiag
						pHeader->iTypeData = 0x030A;
						iIndexBuffer += sizeof(THeader);
						pDateAndTime = (TDateAndTime*) &pBuffer[iIndexBuffer];
						pDateAndTime->iYear 	= HexToDec (pBufferModule[iIndex*20+20]);
						pDateAndTime->iMonth 	= HexToDec (pBufferModule[iIndex*20+21]);
						pDateAndTime->iDay		= HexToDec (pBufferModule[iIndex*20+22]);
						pDateAndTime->iHour		= HexToDec (pBufferModule[iIndex*20+23]);
						pDateAndTime->iMinute	= HexToDec (pBufferModule[iIndex*20+24]);
						pDateAndTime->iSecond	= HexToDec (pBufferModule[iIndex*20+25]);
						pDateAndTime->iMillisecond = HexToDec (pBufferModule[iIndex*20+26])*10+(pBufferModule[iIndex*20+27]/16);
						iIndexBuffer += sizeof(TDateAndTime);
						pData  = (unsigned short int*) &pBuffer[iIndexBuffer];
						*pData = 0x100*pBufferModule[iIndex*20+8]+pBufferModule[iIndex*20+9];	
						iIndexBuffer += sizeof(unsigned short int);
					}
				}
			}
		}
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Create last header for buffer end			if (pFileToView != (FILE*) NULL)

	pHeader = (THeader*) &pBuffer[iIndexBuffer];
	pHeader->iID = 0x0000; // FinBuffer
	pHeader->iTypeData = 0x0000;

	#ifdef S7_DEBUG
	//printf ("\nBuffer length : %d bytes\n",iIndexBuffer+4);
	#endif
	return iErreur;
}
//##########
/*===============================================================================
|	s7GetTextID : return the string corresponding to an ID                      |
|===============================================================================|
|IN  : the ID, file address
|IN+OUT: pointer, where the size of the returned char array is written                                                                  |
|RV  : pointer on the string                                                    |
| The string must be released                                                   |
| 0-pointer means the fonction not found the Id                              |
===============================================================================*/
char* s7GetTextID (unsigned short int iIdNumber, const char* strFile, int* size/*, char* strIdentifiant*/){
	unsigned char bLigne=0;
	int iIndex=0,iDebut,iFin;
	FILE* pFileToView;
	char strBuffer[200];
	char strCode[16];
	char* strIdentifiant = 0;

	sprintf(strCode, "%04X", iIdNumber);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	// open file
	pFileToView=fopen(strFile, "r");
	if (pFileToView != (FILE*) NULL) {
		//printf("  Code to find in %s %c%c%c%c\n",strFile, strCode[0],strCode[1],strCode[2],strCode[3]);
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// First: try to find the same ID (considering also the wildcard "x")
		//        in this case: return the description
		do {
			// Read the first 200 characters of a line
			// after this, the pointer is placed automatically on the beginning of the next line
			fgets(strBuffer,200, pFileToView);
		//	printf("  Code %c%c%c%c\n",strBuffer[0],strBuffer[1],strBuffer[2],strBuffer[3]);
			if (( strBuffer[0]==strCode[0] || strBuffer[0]=='x') &&
				( strBuffer[1]==strCode[1] || strBuffer[1]=='x') &&
				( strBuffer[2]==strCode[2] || strBuffer[2]=='x') &&
				( strBuffer[3]==strCode[3] || strBuffer[3]=='x')) {

				bLigne = 1;
				// Find start index of the string
				do{
					iIndex++;
				} while((strBuffer[iIndex]!=';')&&(iIndex<200));
				iIndex+=2;
				iDebut = iIndex;
				// Find end index of the string
				do{
					iIndex++;
				} while((strBuffer[iIndex]!=';')&&(iIndex<200));
				iFin = iIndex;
			}
			
		} while ((feof(pFileToView)==0) && (bLigne==0));

		fclose(pFileToView);
		if (bLigne == 1) {
			// We found the ID
			*size = ((iFin+1)-iDebut)*sizeof(char);
			strIdentifiant = (char*) malloc(*size);

			for (iIndex = iDebut; iIndex<iFin; iIndex++){
				strIdentifiant[iIndex-iDebut] = strBuffer[iIndex];
			}
			strIdentifiant[iFin-iDebut]='\0';
			
		}
		else {
			// The same ID doesn't exist, find the ID which suits best
			// In this case: return the category
			pFileToView=fopen(strFile, "r"); // open again
			/* The list of the IDs in the file is sorted numerically. So we have to find the
			 * largest ID, which is still less than the desired ID. Since this ID suits
			 * best, we return its category.
			 * The problem: We need the position to line n, but we have to read line n+1 
			 * to find out, which moves the pointer to line n+2. So each time before we read a 
			 * line, we need to save the old position using fsetpos(). */
			fpos_t pos_1; // position of line n+1 (current-1)
			fpos_t pos_2; // position of line n+2 (current-2)
			// Initilize both positions by setting them to the start of the file
			fgetpos(pFileToView, &pos_1);
			pos_2 = pos_1;
			
			do {
				// save the position
				pos_2 = pos_1; // Pos(curr-2) := line n
				fgetpos(pFileToView, &pos_1); // Pos(curr-1) := line n+1
				
				// read next line
				fgets(strBuffer,200, pFileToView);
				// check line: compare the first 4 chars
				if (strncmp(strBuffer, strCode, 4) > 0)
					bLigne = 1;
				
			} while ((feof(pFileToView)==0) && (bLigne==0));
			fclose(pFileToView);
			if (bLigne == 1) {
				/* Ok, we've found a line > ID. But we need to make sure that it isn't the
				 * first line because this means there is no line < ID. Which means there is
				 * no ID in the file which is in the same category as the given ID. */
				if (memcmp(&pos_2, &pos_1, sizeof(fpos_t)) == 0)
					bLigne = 0;
			}
			// Check again: did we find a valid line:
			if (bLigne == 0) {
				// No we didn't. Return "Unknown event"
				*size = 22 *sizeof(char);
				strIdentifiant = malloc( *size );
				sprintf(strIdentifiant, "Unknown event %#04hx", iIdNumber);
				strIdentifiant[*size-1] = '\0';
			}
			else {
				// Yes we did.
				// Find out start and end of the category (the last column of the comma
				// separated values in the text file).
				// start: right after the 3rd semicolon (;)
				// end: end of the line (\n)
				int nSemicolon = 0; // Count the numbers of the semicolon
				do{
					if (strBuffer[iIndex] == ';') 
						nSemicolon ++;
					iIndex++;
					
				} while( iIndex<200 && nSemicolon<3 );
				iDebut = iIndex;
				// Find end index of the string
				do{
					iIndex++;
				} while((strBuffer[iIndex]!='\n')&&(iIndex<200));
				iFin = iIndex - 1; // Don't copy the \n character
							
				// Return text			
				int len = (24 + (iFin+1)-iDebut);
				*size = len *sizeof(char);
				strIdentifiant = malloc( *size );
				
				// h (length) -> short integer (usually not more than 4 digits)
				sprintf(strIdentifiant, "Unknown event %#04hx (%s", iIdNumber, &strBuffer[iDebut]);
				strIdentifiant[len-2] = ')'; // previously \0, because the last char of strBuffer 
				strIdentifiant[len-1] = '\0';
			} // (bLigne == 0) / best suiting ID
		} // if (bLigne == 1) / exact ID
	} // if (pFileToView != (FILE*) NULL)

	//printf("'%s' is the match\n", strIdentifiant);
	return strIdentifiant;
}
