/*
 * driver-library.cpp
 *
 *  Created on: Aug 5, 2013
 *      Author: enice
 */
#include "driver-library.h"

using namespace log4cplus;

extern "C" void initPlcLibrary(const char* logConfigLocation, int loggingConfigurationWatchInterval){
	PlcLogger::initLogging(logConfigLocation, loggingConfigurationWatchInterval);

}

extern "C" PlcInstance* createPlcInstance(int driverType, const char* ipAddress, const char* hostname, const char* category, const char* plogicalName){

	    LOG4CPLUS_DEBUG(Logger::getRoot(),LOG4CPLUS_TEXT("Creating PLC Instance for IP '"<< ipAddress << "' Host Name: '"<<hostname<<"' Category: '"<< category<<"' Logical Name: '"<< plogicalName<<"'"));
		PlcInstance* result = new PlcInstance;
		result->setIpAddress(ipAddress);
		result->setHostName(hostname);
		result->setCategory(category);
		result->setLogicalName(plogicalName);

		string vfdName = string(plogicalName);
		vfdName += "VFD";

		Driver* driver;
		switch(driverType){
				case DRIVER_TYPE_MOCK:
					LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(result),LOG4CPLUS_TEXT("Associated driver MOCK with PLC "<< result->getLogicalName()));
					driver = new DriverMock;
					break;
//				case DRIVER_TYPE_STEP7:
//					LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(result),LOG4CPLUS_TEXT("Associated driver SIEMENS Softnet (STEP7) with PLC '"<< result->getLogicalName()<<"' VFD : '"<<vfdName<<"'"));
//
//					driver = new SoftnetDriver("h1_0",plogicalName, hostname, vfdName);
//					break;
				case DRIVER_TYPE_MODBUS:
					LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(result),LOG4CPLUS_TEXT("Associated driver SCHNEIDER Modbus (UNITY) with PLC '"<< result->getLogicalName()<<"'"));

					driver = new SchneiderDriver;
					driver->setIpAddress(ipAddress);
					break;
				case DRIVER_TYPE_NODAVE:
					LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(result),LOG4CPLUS_TEXT("Associated driver SIEMENS NoDave with PLC '"<< result->getLogicalName()<<"'"));
					driver = new NoDaveDriver(ipAddress, plogicalName);
					break;
				default:
					LOG4CPLUS_FATAL(PlcLogger::getLoggerFromPlcInstance(result),LOG4CPLUS_TEXT("Cannot instantiate PLC "<< result->getLogicalName()<<" : invalid driver type '"<<driverType<<"'"));
					driver = NULL;
					break;

		}
		if(driver != NULL){
		  driver->setCategory(strdup(category));
		  driver->setHostname(strdup(hostname));
		  result->setDriver(driver);
		  return result;
		}else{
			return NULL;
		}

	};

extern "C" int connectPlcInstance(PlcInstance* plcInstance){
	return plcInstance->connect();
}

extern "C" int disconnectPlcInstance(PlcInstance* plcInstance){
	int result = plcInstance->disconnect();
	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(plcInstance),LOG4CPLUS_TEXT("PLC "<< plcInstance->getLogicalName()<<" disconnected with return flag '"<<result<<"'"));
	return result;
}

extern "C" bool isPlcInstanceConnected(PlcInstance* plcInstance){
	return plcInstance->isConnected();
}

extern "C" struct PlcInfoUnionList* queryPlcInstance(PlcInstance* plcInstance){
	PlcInfoUnionList* result = plcInstance->queryInfo();
	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(plcInstance),LOG4CPLUS_TEXT("PLC "<< plcInstance->getLogicalName()<<" completed info query."));
	return result;
}

extern "C" void cleanupPlcInstance(PlcInstance* plcInstance){
	LOG4CPLUS_DEBUG(PlcLogger::getLoggerFromPlcInstance(plcInstance),LOG4CPLUS_TEXT("Cleaning up PLC "<< plcInstance->getLogicalName()));
	free(plcInstance);
}

extern "C" void cleanupPlcInfoUnionList(PlcInfoUnionList* list){
	int i=0;

	if (0 == list->size)
	{
	//	assert(NULL == list->values);
		return;
	}

	assert(NULL != list->values);

	for (i=0; i < list->size; i++)
	{
		if (PLCINFO_STRING == list->values[i].uniontype)
		{
			if(NULL != list->values[i].unionval.stringval){
			  free(list->values[i].unionval.stringval);
			}
		}

		assert(NULL != list->values[i].unionname);
		free(list->values[i].unionname);

	}
	free(list->values);
	free(list);
}
