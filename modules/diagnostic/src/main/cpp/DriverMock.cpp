/*
 * DriverMock.cpp
 *
 *  Created on: Aug 5, 2013
 */

#include "DriverMock.h"
#include <stdlib.h>
#include <string.h>
#include "PlcLogger.h"

DriverMock::DriverMock() {

}

DriverMock::~DriverMock() {
}


int DriverMock::connect(){
	connected = 1;
	return connected;
}


int DriverMock::disconnect(){
	connected = 0;
	return 1;
}


bool DriverMock::isConnected(){
	return (1==connected);

}


PlcInfoUnionList* DriverMock::queryPlcInfo(){

	PlcInfoUnionList* result = initPlcInfoUnionList(3);


	addIntPropertyToList(result,"intProp", 42);

	addFloatPropertyToList(result,"floatProp", 24.0);
	addStringPropertyToList(result,"stringProp","hello");

	return result;
}
