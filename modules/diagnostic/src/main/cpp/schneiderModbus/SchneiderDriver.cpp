#include "schneiderModbus/SchneiderDriver.h"
//#include "agent.h"
#include "schneiderModbus/itemMap.h"
#define SCHNEIDER_RECONNECT	5
//#define	MDB_DEBUG	

/**
 * Constructors/Destructors
 */
/**
 * @param	hostname	Any hostname ("plccois20") or IP address ("137.138.25.72")
 * @param	agentInstance	Diamon agent instance, e.g. "PLC"
 */
SchneiderDriver::SchneiderDriver()
{

	this->reachable = 0;
	hasHwConf = 0;
	reconnectCounter = 1;
	IP="";
	
	resetAllData();
	
	// Create modbus connection object (doesn't connect yet)
	mbConn = new modbusConnection();

	items = new map<string, item*>;
	
	// boolean vars (7)
	addItem(itemMap::getItemByName("PLCRunMode")); // %S12
	addItem(itemMap::getItemByName("PLCIOErr")); // %S10
	addItem(itemMap::getItemByName("PCMCIABAT0")); // %S67
	addItem(itemMap::getItemByName("PLCBAT")); // %S68
	addItem(itemMap::getItemByName("PCMCIABAT1")); // %S75
	addItem(itemMap::getItemByName("RTCERR")); // %S51
	addItem(itemMap::getItemByName("STOPSEC")); // SW54
	addItem(itemMap::getItemByName("STOPHM")); // SW55
	addItem(itemMap::getItemByName("STOPMD")); // SW56
	addItem(itemMap::getItemByName("STOPYEAR")); // SW57
	addItem(itemMap::getItemByName("STOPCODE")); // SW58
	addItem(itemMap::getItemByName("CPUERR")); 
	addItem(itemMap::getItemByName("BLKERRTYPE"));
	addItem(itemMap::getItemByName("MASTPERIOD"));
	addItem(itemMap::getItemByName("MASTCURRTIME")); 
	addItem(itemMap::getItemByName("FASTPERIOD")); 
	addItem(itemMap::getItemByName("FASTCURRTIME")); 
	addItem(itemMap::getItemByName("XWAYNETADDR")); 
	addItem(itemMap::getItemByName("OSCOMMVERS"));
	addItem(itemMap::getItemByName("OSINTVERS")); 
	addItem(itemMap::getItemByName("SEC")); 
	addItem(itemMap::getItemByName("HOURMIN")); 
	addItem(itemMap::getItemByName("MONTHDAY")); 
	addItem(itemMap::getItemByName("YEAR"));
	
//	// get list of all cards

	mbConn->buildPacket(items);
	
	
}

// Destructor
SchneiderDriver::~SchneiderDriver()
{
	
	// disconnect from PLC
	disconnect();
	if (mbConn!= 0 && mbConn->statusConn == 0) {
		delete (mbConn);
	}
	else {
		delete (mbConn);
	}
	// delete items
	map<string, item*>::iterator it; 
	for (it = items->begin(); it != items->end(); it ++) {
		delete it->second;
	}
	
}

/* ******************************
 * Methods
 */
/**
 * Establish a Connection to the PLC
 */
int SchneiderDriver::connect()
{
	if (this->IP == "") {
		
		if (this->IP == "") {
			// still not found
			this->reachable = -1;
			return -1;
		}
	}
	
	// give IP to mbConn
	mbConn->setIp(IP);
	
	mbConn->connect();	
	
	if (mbConn->statusConn == 0) {
		// Connected
		reachable = 1;
	}
	else {
		// Connection failed
		reachable = 0;
	}
	return reachable == 1;
}


/**
 * Set diagnostic data to default values
 */
void SchneiderDriver::resetData()
{	
	redo = 0;
	
}

/**
 * Set all diagnostic Data to default values
 */
void SchneiderDriver::resetAllData()
{
	info = "(no information yet)";
	
	redo = 0;

}

/**
 * Retrieve new data for the tests
 */
struct PlcInfoUnionList* SchneiderDriver::queryPlcInfo() {

	PlcInfoUnionList* result = initPlcInfoUnionList(9);



	// if there is no connection, try to reconnect
	if (mbConn->statusConn != 0) {
		
		// check if I shall try to reconnect this time
		if (reconnectCounter == SCHNEIDER_RECONNECT) {
			// In any case: set counter to 1
			reconnectCounter = 1;
			// yes, try to reconnect
			connect();
			// Was it this time successful?
			if (mbConn->statusConn != 0) {
				
				// still no success: return.
				resetData();
				return NULL;
			}
			
		}
		else {
			// don't try to reconnect, only increment the counter
			reconnectCounter ++;
			resetData();
			return NULL;
		}	
		
	}
	resetData();
	
	if (mbConn->retrieveData() < 0) {
		this->reachable = 0;
		string tmp = " An error occurred while trying to fetch data from the PLC!!!\n";
		tmp += "PLC sent data of an unknown structure!";
		cout << tmp << endl;
		this->disconnect();
		return NULL;
	}
	
	// extract data
	
	int detailValue;
	
	if ((*items)["PLCRunMode"]->value == 1)
     	detailValue = 1;
	else
		detailValue = 0;
	addIntPropertyToList(result,"SC_MODE_RUN",detailValue);

	if ((*items)["PCMCIABAT0"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;
	addIntPropertyToList(result,"SC_MCB_APP",detailValue);

	if ((*items)["PLCBAT"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;
	addIntPropertyToList(result,"SC_BATT",detailValue);


	if ((*items)["PCMCIABAT1"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;
	addIntPropertyToList(result,"SC_MCB_DAT",detailValue);

	if ((*items)["PLCIOErr"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;
	addIntPropertyToList(result,"SC_IE_IO",detailValue);

	if ((*items)["RTCERR"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;
	addIntPropertyToList(result,"SC_DTIME",detailValue);
	
	createInfo();

	addStringPropertyToList(result, "SC_INFO", this->info);
	addStringPropertyToList(result, "SC_DIAG", this->diag);

	addStringPropertyToList(result, "SC_TIME", this->plcCurrentTime);

	/* NOT YET FIGURED OUT
	result->put("SC_CONN",(this->convertToString((*items)["PLCIOErr"]->getStatus())).c_str());
	if ((*items)["PLCIOErr"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;

	result->put("SC_HW",(this->convertToString((*items)["PLCIOErr"]->getStatus())).c_str());
	if ((*items)["PLCIOErr"]->value == 1)
		detailValue = 1;
	else
		detailValue = 0;*/
	
	return result;
	
}


/**
 * Extracts the necessary data from this.items and builds the details
 * string.
 * 
 */
void SchneiderDriver::createInfo() {
	
	
	ostringstream bufInfo;
	ostringstream bufDiag;
	ostringstream bufTime;

	// IP address
	bufInfo << "IP: " << IP << "\n";
	
	// plc current time
	// hex, because it's BCD

	bufTime << "PLC Current Time: ";
	bufTime << hex << setw(2) << setfill('0') << (int) modbusConnection::getLowerByte((*items)["MONTHDAY"]->value) << "/";
	bufTime << hex << setw(2) << setfill('0') << (int) modbusConnection::getHigherByte((*items)["MONTHDAY"]->value) << "/";
	bufTime << hex << setw(4) << setfill('0') << (int) (*items)["YEAR"]->value << " ";
	bufTime << hex << setw(2) << setfill('0') << (int) modbusConnection::getHigherByte((*items)["HOURMIN"]->value) << ":";
	bufTime << hex << setw(2) << setfill('0') << (int) modbusConnection::getLowerByte((*items)["HOURMIN"]->value) << ":";
	bufTime << hex << setw(2) << setfill('0') << (int) modbusConnection::getHigherByte((*items)["SEC"]->value);
	bufTime << "\n";

	
	// commercial version
	bufInfo << (*items)["OSCOMMVERS"]->description << ": ";
	bufInfo << hex << (int) modbusConnection::getHigherByte((*items)["OSCOMMVERS"]->value) << ".";
	bufInfo << hex << setw(2) << setfill('0') << (int) modbusConnection::getLowerByte((*items)["OSCOMMVERS"]->value);
	bufInfo << "\n";
	
	// firmware version
	bufInfo << (*items)["OSINTVERS"]->description << ": ";
	bufInfo << (int) (*items)["OSINTVERS"]->value;
	bufInfo << "\n";
	bufInfo << "\n"; // empty line
	
	// last stop
	bufDiag << "PLC Last Stop: ";
	
	uchar stopCode = modbusConnection::getLowerByte((*items)["STOPCODE"]->value);
	if(stopCode > 0x06){
		bufDiag << "N/A (Code "<< itemMap::getValueDesc((*items)["STOPCODE"]->typeS, (*items)["STOPCODE"]->address, modbusConnection::getLowerByte((*items)["STOPCODE"]->value));
		bufDiag << ") \n";

	}else{
		// time
		bufDiag << hex << setw(2) << setfill('0') << (int) modbusConnection::getLowerByte((*items)["STOPMD"]->value) << "/";
		bufDiag << hex << setw(2) << setfill('0') << (int) modbusConnection::getHigherByte((*items)["STOPMD"]->value) << "/";
		bufDiag << hex << setw(4) << setfill('0') << (int) (*items)["STOPYEAR"]->value << " ";
		bufDiag << hex << setw(2) << setfill('0') << (int) modbusConnection::getHigherByte((*items)["STOPHM"]->value) << ":";
		bufDiag << hex << setw(2) << setfill('0') << (int) modbusConnection::getLowerByte((*items)["STOPHM"]->value) << ":";
		bufDiag << hex << setw(2) << setfill('0') << (int) modbusConnection::getHigherByte((*items)["STOPSEC"]->value);
		bufDiag << " (Code ";
		// code
		bufDiag << itemMap::getValueDesc((*items)["STOPCODE"]->typeS, (*items)["STOPCODE"]->address, modbusConnection::getLowerByte((*items)["STOPCODE"]->value));
		bufDiag << ") \n";
	}

	// cpu error
	bufDiag << (*items)["CPUERR"]->description << ": ";
	// only add description when there is indeed an error (value!=0)
	if ((*items)["CPUERR"]->value != 0) {
		bufDiag << itemMap::getValueDesc((*items)["CPUERR"]->typeS, (*items)["CPUERR"]->address, modbusConnection::getLowerByte((*items)["CPUERR"]->value));
	}
	else {
		bufDiag << "0";
	}
	bufDiag << "\n";

	// block error
	bufDiag << (*items)["BLKERRTYPE"]->description << ": ";
	bufDiag << itemMap::getValueDesc((*items)["BLKERRTYPE"]->typeS, (*items)["BLKERRTYPE"]->address, (*items)["BLKERRTYPE"]->value);
	bufDiag << "\n";
	bufDiag << "\n"; // empty line

	// mast cycle
	bufDiag << (*items)["MASTPERIOD"]->description << ": ";
	bufDiag << dec << (int) modbusConnection::getLowerByte((*items)["MASTPERIOD"]->value);
	bufDiag << " ms \n";

	bufDiag << (*items)["MASTCURRTIME"]->description << ": ";
	bufDiag << dec << (int) (*items)["MASTCURRTIME"]->value;
	bufDiag << " ms \n";

	// fast cycle
	bufDiag << (*items)["FASTPERIOD"]->description << ": ";
	bufDiag << dec << (int) modbusConnection::getLowerByte((*items)["FASTPERIOD"]->value);
	bufDiag << " ms \n";

	bufDiag << (*items)["FASTCURRTIME"]->description << ": ";
	bufDiag << dec << (int) (*items)["FASTCURRTIME"]->value;
	bufDiag << " ms \n";
	bufDiag << "\n"; // empty line

	// xway address
	bufInfo << (*items)["XWAYNETADDR"]->description << "/Station number (Premium only): ";
	bufInfo << dec << (int) modbusConnection::getLowerByte((*items)["XWAYNETADDR"]->value);
	bufInfo << "\n";

	bufInfo << (*items)["XWAYNETADDR"]->description << "/Network number (Premium only): ";
	bufInfo << dec << (int) modbusConnection::getHigherByte((*items)["XWAYNETADDR"]->value);
	bufInfo << "\n";

	info = "N/A";
	diag = "N/A";
	plcCurrentTime = "N/A";
	info = bufInfo.str();
	diag = bufDiag.str();
	plcCurrentTime = bufTime.str();
	
}


void SchneiderDriver::addItem(item* i) {
	(*items)[i->name] = i;
}


string SchneiderDriver::convertToString(int number){
	std::stringstream ss;

	ss << number;

	return ss.str();
}

int SchneiderDriver::disconnect(){

	if(this->mbConn->statusConn==0){
		this->mbConn->close();
		this->reconnectCounter=0;
		this->mbConn->statusConn=-1;
		this->reachable = 0;
		return 1;
	}else{
		return 0;
	}
}

bool SchneiderDriver::isConnected(){
	if(this->mbConn->statusConn==0)
		return true;
	else
		return false;
}


