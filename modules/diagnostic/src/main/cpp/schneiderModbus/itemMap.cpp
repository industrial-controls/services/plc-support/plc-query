#include "schneiderModbus/itemMap.h"
//#include "agent.h"
#include "Driver.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <unistd.h>
//#define MDB_DEBUG

string 		itemMap::pathHwConfig;

itemMap::itemMap()
{
}

itemMap::~itemMap()
{
}

/**
 * Returns an item with the provided name, e.g. "PLCIOErr".
 * @return	item	with filled properties (on success), 
 * 					with empty properties (on error)
 */
item* itemMap::getItemByName(string name) {
	string line;
	bool found = false;

	ifstream myfile (ITEMS_FILENAME);
	if (myfile.is_open()) {
		while (! myfile.eof() ) {
			getline (myfile, line);
			
			if (line.find(name) == 1) {
				found = true;
				break;
			}
		}
		myfile.close();
	}
	else {
		cout << "Unable to open file " << ITEMS_FILENAME << " - fatal - killing the agent";
		exit(0);
	}
	
	if (found) {
		// extract desc and address
		// find end of desc (= 4. ")
		// "name","desc",...
		int start = name.length()+4;
		int end = line.find('"', start);
		// params: start index, length
		string desc;
		desc = line.substr(start, end-start);
#ifdef MDB_DEBUG
		cout << "Desc: " << desc << endl;
#endif
		
		// extract type ("S",...)
		start = end + 3;
		end = line.find('"', start);
		string typeS = line.substr(start, end-start);
#ifdef MDB_DEBUG
		cout << "Type: " << typeS << endl;
#endif
		
		// extract addr
		start = end + 3;
		end = line.find('"', start);
		
		istringstream istr;
		istr.str( line.substr(start, end-start) );
		uint addr;
		istr >> addr;
#ifdef MDB_DEBUG
		printf("Addr: %d\n", addr);
#endif
		
		// find out length
		uint length;
		if (typeS.find('W',0) == string::npos) {
			// no "W" --> bool / 1 Byte
			length = 1; 
		}
		else {
			// contains a "W" --> word/(u)int / 2 Bytes
			length = 2;
		}
		
		// create item with these properties
		item* i = new item(name);
		// get type (int)
		i->type = (uint) getType(typeS);
#ifdef MDB_DEBUG
		printf("\n");
#endif
		
		i->typeS = typeS;
		i->address = addr;
		i->length = length;
		i->description = desc;
		i->setPadding();
		
		if (i->type == 0) {
			// for %S: set "okValue"
			i->setOkValue();
		}
		
		return i;
		
	}
	else {
		// Agent::mainLog->errorStream() << "Could not find item: " << name ;
		return new item("");
	}
	
	
}

/**
 * Returns an item type as int.
 * @arg		type	e.g. "SW"
 * @return	0..n (success) or UINT_MAX on error
 */
uint itemMap::getType(string type) {
	// Look for type in the file
	string line;
	bool found = false;
	
	ifstream myfile (ITEMS_TYPES);
	if (myfile.is_open()) {
		while (! myfile.eof() ) {
			getline (myfile, line);
			
			if (line.find( type + "," ) == 0) {
				found = true;
				//cout << line << endl;
				break;
			}
		}
		myfile.close();
	}
	else {
		// Agent::mainLog->errorStream() << "Unable to open file" ;
		return UINT_MAX;
	}
	if (found) {
		int start = line.find(",", 0 ) + 1;
		int end = line.find(".", start );
		istringstream istr;
		istr.str(line.substr(start, end-start));
		int type;
		istr >> type;
#ifdef MDB_DEBUG
		printf("Type: %d\n", type);
#endif
		
		return type;
	}

	// Agent::mainLog->errorStream() << "Unknown type " << type << ".";
	return UINT_MAX;

}

/**
 * returns true, if there is further information available on the value.
 * e.g. %SW58 = 6 -> Stop on software fault
 * 
 */
bool itemMap::hasValueDesc(string type, uint addr) {
	
	if (type == "SW") {
		
		if (addr == 58)
			return true;
		else if (addr == 124)
			return true;
		else if (addr == 125)
			return true;
		
	}
	
	return false;
}

/**
 * Returns a string describing the value of this item.
 * e.g. %SW58 = 6 -> "0x0006: Stop on software fault"
 */
string itemMap::getValueDesc(string type, uint addr, uint value) {
	
	char searchstring[32];
	
	// important: capital letters
	sprintf(searchstring, "\"%%%s%u\",\"%X\"", type.c_str(), addr, value);
	
	string searchS(searchstring);
	
	// Look for type in the file
	string line;
	bool found = false;
	
	ifstream myfile (ITEMS_DETAILS);
	if (myfile.is_open()) {
		while (! myfile.eof() ) {
			getline (myfile, line);
			
			if (line.find(searchS) == 0) {
				found = true;
				//cout << line << endl;
				break;
			}
		}
		myfile.close();
	}
	else {
		// Agent::mainLog->errorStream() << "Unable to open file" ;
	}
	string desc;
	if (found) {
		int start = searchS.size() + 2;
		int end = line.find("\"", start + 1 );

		desc = line.substr(start, end-start);

#ifdef MDB_DEBUG
		cout << "Desc: " << desc << endl;
#endif
	}
	else {
		desc = "unknown";
	}
	ostringstream buf;
	buf << "0x" << hex << setw(4) << setfill('0') << (int) value << ": " 
		<< desc;
	return buf.str();

}


/**
 * Returns details for an item, which do not depend on the value.
 */
string itemMap::getDetails(item* it) {
	
	char searchstring[32];
	
	// important: capital letters
	sprintf(searchstring, "\"%%%s%u\",\"x\"", it->typeS.c_str(), it->address);
	
	string searchS(searchstring);
	
	// Look for type in the file
	string line;
	bool found = false;
	
	ifstream myfile (ITEMS_DETAILS);
	if (myfile.is_open()) {
		while (! myfile.eof() ) {
			getline (myfile, line);
			
			if (line.find(searchS) == 0) {
				found = true;
				//cout << line << endl;
				break;
			}
		}
		myfile.close();
	}
	else {
		// Agent::mainLog->errorStream() << "Unable to open file" ;
	}
	if (found) {
		int start = searchS.size() + 2;
		int end = line.find("\"", start + 1 );
		string desc;
		desc = line.substr(start, end-start);

#ifdef MDB_DEBUG
		cout << "Desc: " << desc << endl;
#endif
		// Replace "\n" by real linebreaks.
		desc = stringReplace(desc, "\\n", "\n");
		// Write the address in the first line
		desc = it->getItemAddrComplete() + "\n" + desc;
		
		
		return desc;
	}
	// Nothing found or file cannot be opened:
	//ostringstream buf;
	//buf << "0x" << hex << setw(4) << setfill('0') << (int) value << " (unknown)";
	// Agent::mainLog->noticeStream() << "No details found" ;
	return "";
}





/**
 * Checks, if the file exists.
 */
bool itemMap::fileExists(string path) {
	
	if (FILE* file = fopen(path.c_str(), "r")) {
	        fclose(file);
	        // file exists
	        return true;
    }
    else {
    	// file doesn't exist
    	return false;
    }
}



string itemMap::stringReplace(string myString, string search, string replace) {
  int pos;
  while (true) {
    pos = myString.find(search);
    if (pos == -1) {
      break;
    } else {
    	myString.erase(pos, search.length());
    	myString.insert(pos, replace);
    }
  }
  return myString;
}

