#include "schneiderModbus/item.h"

#include <iomanip>
#include <sstream>
#include <iostream>
#include <string>

using namespace std;

/* Constructors */
item::item()
{ 
	type = 0;
	value = 0;
	address = 0;
	length = 0;
}

item::item(string name)
{
	this->name = name;
	value = 0;
	
}

item::~item()
{
}

/**
 * @return	e.g. "%SW51"
 */
string item::getItemAddrComplete() const
{
	ostringstream buf;
	buf << "%" << typeS << (int) address;
	return buf.str();
}

void item::setPadding() {
	
	if (type == 1 && address >= 180) {
		// for SW180, SW181 etc.
		paddingLength = 0;	
	}
	else {
		paddingLength = 2;	
	}
}

void item::setOkValue() {
	// By now, only applicable for %S
	if (type == 0) {
		switch (address) {
		case 12: 
			okValue = 1;
			break;
		case 10:
			okValue = 1;
			break;
		case 67:
			okValue = 0;
			break;
		case 68:
			okValue = 0;
			break;
		case 75:
			okValue = 0;
			break;
		case 51:
			okValue = 0;
			break;
		case 16:
			okValue = 1;
			break;
		default:
			return;

		}	
	} // if (type == 0)
}

int item::isOk() {
	
	return (int) (value == okValue);
}

uint item::getStatus() {
	
	if (value == okValue) {
		return OK;
	}
	// not ok
	
	if (type == 0 && address == 16) {
		// %S16
		return WARN;
	}
	// else
	return ERR;
	
	
}

