#include "schneiderModbus/modbusConnection.h"
#include <iostream>
#include <unistd.h>

#include "PlcLogger.h"


using namespace std;
using namespace log4cplus;

// socket timeout for sending and receiving in seconds
#define SOCKET_TIMEOUT_SEC 	2
using namespace std;
extern "C" {
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
    #include<errno.h>
}


modbusConnection::modbusConnection()
{
	statusConn = -2;
	
	// minimal length: the header size
	lengthIn = lengthHeaderIn; // see below
	lengthOut = lengthHeaderOut; // TA(2)+prot.(2)+len(2)+fc(2)+sfc(2)+no(1)
	// Set pointer to 0
	dataOut = 0;
	dataIn = 0;
	transactionId = 0;
	
	
}

/* Destructor */
modbusConnection::~modbusConnection()
{
	free(dataOut);
	free(dataIn);
	close();
}

/**
 * Creates a TCP socket to the plc.
 * 
 */
void modbusConnection::connect()
{
    int connectReturnFlag = 0;
	createSocket();
	
	struct sockaddr_in address;
    
	
	if (socketId <= 0) {
		
		LOG4CPLUS_ERROR(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Creation of the Socket failed. Operating System error code : "<<socketId<<" - Connection status is "<<statusConn<<" : ERRNO "<<errno));
    
        return;

	}
	

    address.sin_family = AF_INET;
    address.sin_port = htons (PORT_MODBUS);
    inet_aton (IP.c_str(), &address.sin_addr);
    
    
    connectReturnFlag = ::connect ( socketId,
                    (struct sockaddr *) &address,
                    sizeof (address));
    if (connectReturnFlag != 0) {
        
        // Connection error
		if(failedToConnect==true){
			this->iReconnectionTryNumber++;
		}
		LOG4CPLUS_ERROR(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Connection of the socket failed. Operating System error code : "<<connectReturnFlag));

		close();
        statusConn = -1;
        return;
    }
    // Successfully connected
    statusConn = 0;

    // TODO Add more information about the PLC connection
    LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Successfully connected"));

	return;

}



/**
 * Create socket and set options.
 */
void modbusConnection::createSocket() {
	
	// create socket (don't connect yet)
	if ((socketId = socket(AF_INET, SOCK_STREAM, 0)) <= 0) {
		// Error

		statusConn = -1;
		LOG4CPLUS_ERROR(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Modbus : could not create socket."));
		failedToConnect=true;
		return;
	}
	// Set timeout for sending and receiving packets
	struct timeval socketTimeout;
	socketTimeout.tv_sec = SOCKET_TIMEOUT_SEC;
	socketTimeout.tv_usec = 0;
	
	// Setting this timeout prevents the program from blocking a looooong time on
	// connect() and recv()
	setsockopt(socketId, SOL_SOCKET, SO_RCVTIMEO, &socketTimeout, sizeof(struct timeval));
	setsockopt(socketId, SOL_SOCKET, SO_SNDTIMEO, &socketTimeout, sizeof(struct timeval));
	
}


/**
 * Closes the TCP socket.
 * 
 */
void modbusConnection::close()
{
    if(socketId > 0){
	   ::close(socketId);
	   LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Closed socket"));
    }
}

/**
 * Asks the PLC for the desired data.
 * 
 */
int modbusConnection::retrieveData()
{
    LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Querying data.."));
	// Add transaction ID
	uint ta = getTransactionId();
	dataOut[0] = getHigherByte(ta);
	dataOut[1] = getLowerByte(ta);
	
	// If necessary, free the memory that was previously allocated
	if (dataIn != 0)
		free(dataIn);
	// Allocate memory to which the sendData() method can write
	dataIn = (uchar*) malloc( (lengthIn + 1) * sizeof(uchar) );
	
	if (sendData(dataOut, lengthOut, dataIn, lengthIn) == 0) {
		// OK
		extractData();
	    LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Data query successful.."));
		return 0;
	}
	else {
	    LOG4CPLUS_ERROR(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Could not query data."));
		// Error
		return -1;
	}
	
	

}

/**
 * Sends any desired data and writes the response in the provided char array.
 * 
 * @param	dataOut		Data to send
 * @param 	lengthOut	Length of outgoing data
 * @param	*dataIn		Pointer to some free space where the response will be 
 * 						written
 * @param	expectedLength
 * 
 */
int modbusConnection::sendData(const uchar* dataOut, int lengthOut, uchar* dataIn, int expectedLength) {
	// variable required in debug mode
	send(socketId, dataOut, lengthOut, 0);
		
#ifdef MDB_DEBUG	
	for (uint i = 0; i < (uint)lengthOut; i ++) {
		printf("  %02x", dataOut[i]);
	}
	printf("\n");
	for (uint i = 0; i < (uint)lengthOut; i ++) {
		printf(" % 3u", dataOut[i]);
	}

	printf("\n");
	//printf("%d Bytes sent. \n", sizeSent);
#endif
	
	// Receive response
	int size = recv(socketId, dataIn, expectedLength, 0);
		 
	if( size > 0) {
		dataIn[size] = '\0';
		
#ifdef MDB_DEBUG		
		printf ("Received message (%d B), %u B expected: \n", size, expectedLength);
	
		for (int i = 0; i < size; i ++) {
			printf("  %02x", dataIn[i]);
		}
		printf("\n");
		for (int i = 0; i < size; i ++) {
			printf(" % 3u", dataIn[i]);
		}
		printf("\n");
#endif	
		// check received size
		if ( size == expectedLength) {
			// ok
			return 0;
		}
		else {

			// Agent::mainLog->debug("%s Expected %u Bytes, but received %d! Abort.", IP.c_str(), expectedLength, size);
			LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Modbus "<<IP.c_str()<<" Expected "<<expectedLength<<" Bytes, but received "<<size<<" ! Abort."));
			// If we didn't receive anything (0 B), there is probably a network error
			// Setting statusConn=-1 will create a new socket next time
			if (size == 0) {
				statusConn = -1;
				LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Modbus "<<IP.c_str()<<" Requesting a reconnection for next diagnostic monitoring cycle."));
			}
			return -1;
		}
	}
	else {
		// size < 0:
		statusConn = -1;
		LOG4CPLUS_INFO(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Modbus "<<IP.c_str()<<" sendData() error."));

		return -1;
	}
}


/**
 * Builds a modbus packet based on map this->items.
 * Since this is a map, the items are ordered in another order
 * than they were added to the map!
 * 
 */
void modbusConnection::buildPacket(map<string, item*>* itemsMap)
{

	items = itemsMap;
	// calculate outgoing and incoming length
	
	map<string, item*>::iterator it; 
	for (it = items->begin(); it != items->end(); it ++) {
		lengthOut += 8; // +type(2)+addr(2)+padding(4)
		lengthIn += it->second->length + it->second->paddingLength; //

	}
	
	// First: allocate memory (and free the old one, if necessary)
	free(dataOut);
	dataOut = (uchar*) malloc( (lengthOut + 1) * sizeof(uchar) );
	
	// write the header
	//uchar* ptr;
	//ptr = dataOut;
	
	// transaction id is set in retrieve data (index 0-1)
	// (so the packet has to be built only once)
	
	dataOut[2] = 0;
	dataOut[3] = 0;
	dataOut[4] = getHigherByte(lengthOut-6);
	dataOut[5] = getLowerByte(lengthOut-6);
	dataOut[6] = 0;
	dataOut[7] = 0x5a; // function code
	dataOut[8] = 0;
	dataOut[9] = 0x24; // subfunction code
	dataOut[10] = (uint) items->size();
	
	// iterate over alle items and write the data to the packet
	uchar* ptr;
	ptr = &(dataOut[11]);
	
	for (it = items->begin(); it != items->end(); it ++) {
		// type
		*ptr = 0;
		ptr ++;
		*ptr = it->second->type; // type is usually not larger than one Byte
		//printf("type: %u\n", it->second.type);
		ptr ++;
		// addr
		*ptr = getLowerByte(it->second->address); // first the lower byte!
		ptr ++;
		*ptr = getHigherByte(it->second->address);
		ptr ++;
		// padding (00 00 01 00)
		*ptr = 0;
		ptr ++;
		*ptr = 0;
		ptr ++;
		*ptr = 1;
		ptr ++;
		*ptr = 0;
		ptr ++;
	}	
}

/**
 * Increments the internal transaction ID an returns the new value
 * 
 */
uint modbusConnection::getTransactionId()
{
	/* Don't rely on the size of data types (which might vary on different
	 * computers. Make sure yourself, that the value never exceeds 2 Bytes. */
	if (transactionId == 0xFFFF) {
		transactionId = 0;
	}
	else {
		transactionId ++;
	}
	return transactionId;
}

/**
 * Returns the LOWER Byte of the provided number (which itself is not
 * more than 2 Bytes long).
 */
uchar modbusConnection::getLowerByte(uint number) {
	return (uchar) (number % 0x100);
}

/**
 * Returns the HIGHER Byte of the provided number (which itself must not
 * be longer than 2 Bytes).
 */
uchar modbusConnection::getHigherByte(uint number) {
	return (uchar) ( (number >> 8) % 0x100 );
}

/**
 * Extracts the data from the recently received packet.
 * 
 */
void modbusConnection::extractData() {
	// iterate over all items and write the data to the packet

	int cnt = 0;
	uchar* ptr;
	ptr = &(dataIn[10]);
	
	map<string, item*>::iterator it; 
	for (it = items->begin(); it != items->end(); it ++) {
		ptr ++;
		ptr += it->second->paddingLength;
		if (it->second->length == 1) {
			// boolean
			// if received value is 3, then only save 1
			it->second->value = (uint)*ptr % 2;
		}
		else {
			// length == 2
			// LOW byte comes first
			it->second->value = (uint)*ptr;

			ptr ++;
			// HIGH byte comes last
			// move the value to the higher byte
			it->second->value += (uint)*ptr << 8;
			
		}
		cnt++;
	}

    LOG4CPLUS_DEBUG(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Received data for "<<cnt<<" item(s)."));

}

/**
 * @return	A copy of the current item list
 * 
 * !! NB: The items are not returned in the same order as added!
 * However, they can be accessed using items["name"]
 */
map<string, item*>* modbusConnection::getItems() {
	return items;
}

/**
 * STOPS the execution of the plc
 */
void modbusConnection::stopPlc() {
	
	uchar dataStop[] = {
		0x00, 0x00,  0x00, 0x00,  0x00, 0x06, 
		0x00, 0x5a, 0x00, 0x41,
		0xff, 0x00
	};
	
	// Add transaction ID
	uint ta = getTransactionId();
	dataStop[0] = getHigherByte(ta);
	dataStop[1] = getLowerByte(ta);
	
	// btw: the expected response is: 00 00 00 00 00 04 00 5a 00 fe.
	// but we don't care about the response.
	// We just have to provide some space to write it to.
	uchar response[10];
	
	// send
	sendData(dataStop, 12, response, 10);
	
}

/**
 * STARTS the execution of the plc
 */
void modbusConnection::startPlc() {
	
	uchar dataStart[] = {
		0x00, 0x00,  0x00, 0x00,  0x00, 0x06, 
		0x00, 0x5a, 0x00, 0x40,
		0xff, 0x00
	};
	
	// Add transaction ID
	uint ta = getTransactionId();
	dataStart[0] = getHigherByte(ta);
	dataStart[1] = getLowerByte(ta);
	
	// btw: the expected response is: 00 00 00 00 00 04 00 5a 00 fe.
	// but we don't care about the response.
	// We just have to provide some space to write it to.
	uchar response[10];
	
	// send
	sendData(dataStart, 12, response, 10);
	
}

void modbusConnection::setIp(string IP) {
	
	this->IP = IP;
}
/**
 * Send a packet to read status of this card.
 */
int modbusConnection::getCardStatus(int rack, int slot) {
	uint ta = getTransactionId();
	
	// 27 Bytes request
	uchar dataRequest[] = {
		getHigherByte(ta), getLowerByte(ta),
		0x00, 0x00, 0x00, 0x15, 0x00, 0x5a, 0x00, 0x70, 0x04, 	
		getHigherByte((uint) rack), getLowerByte((uint) rack),
		getHigherByte((uint) slot), getLowerByte((uint) slot),
		0x00, 0x01, 0x30, 0x06, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00
	};
	uchar dataResponse[20];

	int result = sendData(dataRequest, 27, dataResponse, 20);
	if (result < 0) {
		// The response frame is shorter than expected
		// --> probably this slot is empty
		return PR_CARD_NA;
	}

	// Card exists
	switch (dataResponse[18]) {
	case 0x08:
		return PR_CARD_NOTOK;
	case 0x02:
		return PR_CARD_NOTOK;
	case 0x06:
		return PR_CARD_NOTOK;
	case 0x10:
		return PR_CARD_NOTPRESENT;
	case 0x00:
		return PR_CARD_OK;
	case 0x04:
		return PR_CARD_CONNECTOR_FAULT;
	default:
		LOG4CPLUS_INFO(Logger::getInstance("plc.schneider.modbus"), LOG4CPLUS_TEXT("Modbus "<<IP.c_str()<<" Unknown card status: "<<dataResponse[18]<<" (rack "<<rack<<", slot "<<slot<<")"));
		return -1;
	}

}




