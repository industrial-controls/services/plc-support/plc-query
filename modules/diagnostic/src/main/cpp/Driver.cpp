/*
 * Driver.cpp
 *
 *  Created on: Aug 5, 2013
 *      Author: enice
 */

#include "Driver.h"
#include <string.h>

Driver::Driver() {
	IP = "";

}

Driver::~Driver() {
}


string Driver::getIpAddress()
{
	return IP;
}

void Driver::setIpAddress(string ipAddress)
{
	IP = ipAddress;
}

string Driver::getHostname()
{
	return this->hostname;
}
void Driver::setHostname(string hostname)
{
	this->hostname = hostname;
}

string Driver::getCategory(){
	return this->category;
}
void Driver::setCategory(const char* category){
	this->category = strdup(category);
}


void Driver::addStringPropertyToList(PlcInfoUnionList* list, const string& propertyName, const string& propertyValue){
	list->values[list->size].uniontype=PLCINFO_STRING;
	list->values[list->size].unionname = strdup(propertyName.c_str());;
	list->values[list->size].unionval.stringval = strdup(propertyValue.c_str());
	list->size++;
}

void Driver::addFloatPropertyToList(PlcInfoUnionList* list, const string& propertyName, float propertyValue){
	list->values[list->size].uniontype = PLCINFO_FLOAT;
	list->values[list->size].unionname = strdup(propertyName.c_str());;

	list->values[list->size].unionval.floatnumber =propertyValue;
	list->size++;

}
void Driver::addIntPropertyToList(PlcInfoUnionList* list, const string& propertyName, int propertyValue){
	list->values[list->size].uniontype = PLCINFO_INTEGER;
	list->values[list->size].unionname = strdup(propertyName.c_str());;

	list->values[list->size].unionval.intnumber = propertyValue;
	list->size++;
}

PlcInfoUnionList* Driver::initPlcInfoUnionList(int maxSize){


	PlcInfoUnionList* result = new PlcInfoUnionList;
	result->values = (PlcInfoUnionHolder*)malloc(sizeof(PlcInfoUnionHolder) * maxSize);
	memset(result->values, 0, sizeof(PlcInfoUnionHolder) * maxSize);
	result->size = 0;

	return result;
}
