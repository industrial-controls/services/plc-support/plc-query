#ifndef ITEM_H_
#define ITEM_H_

#include <string>

typedef unsigned int	uint;
typedef unsigned char	uchar;

using namespace std;

/**
 * An item represents a variable in a Schneider PLC with all its properties
 * (name, type, address, etc.)
 */
class item
{
public:
	item();
	/**
	 * Creates a new item object.
	 * Items are usually created by the itemMap, which also retrieves the
	 * properties from a text file.
	 */
	item(string name);
	virtual ~item();
	
	/**
	 * A constant to indicate the status "error".
	 */
	static const uint ERR	= 0;
	/**
	 * A constant to indicate the status "ok".
	 */
	static const uint OK 	= 1;
	/**
	 * A constant to indicate the status "warning".
	 */
	static const uint WARN 	= 2;
	/**
	 * A constant to indicate the status "fatal".
	 */
	static const uint FATAL	= 3;
	
	/**
	 * E.g. "PLCRunMode"
	 */
	string name;
	
	/**
	 * The type of the item as string, e.g. "S", "SW", ...
	 */
	string typeS;
	
	/**
	 * The type of the item as integer, e.g. 0, 1, 2, ...
	 */
	uint type; 
	
	/**
	 * The value of this memory address.
	 */
	uint value; 
	
	/**
	 * If 0 is the "good" (OK) value, this variable is set to 0, and vice versa.
	 * By now, only applicable for %S.
	 */
	uint okValue;
	
	/**
	 * May contain further information to the item's value.
	 */
	string valueDesc;
	
	/**
	 * Address as int,  e.g. 12
	 */
	uint address;
	
	/**
	 * Length in bytes, normally 1 or 2
	 */
	uint length;
	
	/**
	 * Number of padding bytes in the answer that are put before the value.
	 * In most times it is 2, sometimes 0.
	 */
	uint paddingLength;
	
	/**
	 * Description
	 */
	string description;
	
	/**
	 *  Returns a string like "%SW51"
	 */
	string getItemAddrComplete() const;
	
	/**
	 * Sets paddingLength with the help of address and type.
	 */
	void setPadding();
	
	/**
	 * Sets okValue with the help of address and type.
	 */
	void setOkValue();
	
	/**
	 * Returns 1 if value == okValue.
	 * Returns 0 if value != okValue.
	 */
	int isOk();
	
	/**
	 * Returns the status.
	 * 
	 * @return	OK, ERR, ...
	 */
	uint getStatus();
	
};

#endif /*ITEM_H_*/
