#ifndef ITEMMAP_H_
#define ITEMMAP_H_
#include "item.h"
#include <string>
#include <vector>
#include <map>
#include <climits>
//#include <xercesc/dom/DOM.hpp>
//#include <xercesc/parsers/XercesDOMParser.hpp>
using namespace std;
// indicate using Xerces-C++ namespace in general
//XERCES_CPP_NAMESPACE_USE
#define ITEMS_FILENAME 	"./itemdesc.txt"
#define ITEMS_TYPES 	"./itemtypes.txt"
#define ITEMS_DETAILS 	"./itemdetails.txt"

/**
 * This static class operates like a factory. Invoked with a name such as 
 * "PLCRunMode", it looks up the remaining data (address, length, ...) in the
 * text file and returns the desired item.
 *
 */
class itemMap
{

public:
	
	virtual ~itemMap();
	
	/**
	 * The path for the xml fileswith the hardware configuration
	 * is read from config.txt and stored here to avoid file
	 * access for every PLC.
	 */
	static string pathHwConfig;	
	
	
	/**
	 * The serializer to write an xml tree to a string
	 */
	//static DOMWriter* theSerializer;

	/**
	 * Returns an item with the provided name, e.g. "PLCIOErr".
	 * 
	 * @return	item	with filled properties (on success), 
	 * 					with empty properties (on error)
	 */	
	static item* getItemByName(string name);

	/**
	 * Returns an item type as int. Normally not required by other classes.
	 * 
	 * @arg		type	e.g. "SW"
	 * @return	0..n (success) or UINT_MAX on error
	 */	
	static uint getType(string type);
	

	/**
	 * returns true, if there is further information available on the value.
	 * e.g. %SW58 = 6 -> Stop on software fault
	 */	
	static bool hasValueDesc(string type, uint addr);

	/**
	 * Returns a string describing the value of this item.
	 * e.g. %SW58 = 6 -> "Stop on software fault"
	 */
	static string getValueDesc(string type, uint addr, uint value);
	
	/**
	 * Returns the description of an item, which is independent from the value.
	 */
	static string getDetails(item* it);
	
	/**
	 * Returns a file name with path to the file which contains the hw config data.
	 * This method checks, if the file exists, otherwise it returns "".
	 * If the file doesn't exist, it also tries a filename with small letters etc..
	 * Filename should not start with a capital letter, when the rest is small:
	 * Plccois25.xhw. But these are ok: plccois25.xhw, PLCCOIS.xhw,...
	 * "-" in the hostname will be changed to "_" in the filename.
	 * 
	 * @param	hostname	The hostname in small or capital letters.
	 * @return	Complete path with filename to the file (file exists) or "" (doesn't exist)
	 * 
	 */
	static string getFilename(string hostname);
	
	/**
	 * Checks, if the file exists.
	 */
	static bool fileExists(string path);
	  
	/**
	 * Wrapper method that returns the desired attribute of the provided node.
	 */
//	static string getAttribute(const DOMNode* node, const char* attrib);

	/**
	 * Returns a pointer to the root node of the XML tree (PREMIUM
	 * AND QUANTUM). Invokes either getHwConfigPremium(...) or
	 * getHwConfigQuantum(...).
	 * PREMIUM: Only creates items for power supply!
	 * 
	 * @param hostname 	Hostname of the PLC, necessary for the filename
	 * 					of the XML file with the hardware config data.
	 * @param items 	Items map to append the necessary items.
	 * @param impl		Required for the creation of the target tree.
	 */
//	static DOMElement* getHwConfig(string hostname, map<string, item*>* items, DOMImplementation* impl);
	
	/**
	 * Returns a pointer to the root node of the XML tree containing the
	 * racks > cards. ---> PREMIUM
	 * Creates an item for all modules (if necessary).
	 * 
	 * @param doc		Document of the source tree.
	 * @param items 	Items map to append the necessary items.
	 * @param impl		Required for the creation of the target tree.
	 */
//	static DOMElement* getHwConfigPremium(DOMDocument* doc, map<string, item*>* items, DOMImplementation* impl);
	
	/**
	 * Returns a pointer to the root node of the XML tree containing the
	 * station > racks > cards. ---> QUANTUM
	 * 
	 * @param doc		Document of the source tree.
	 * @param items 	Items map to append the necessary items.
	 * @param impl		Required for the creation of the target tree.
	 */
//	static DOMElement* getHwConfigQuantum(DOMDocument* doc, map<string, item*>* items, DOMImplementation* impl);
	
	/**
	 * Replaces all occurences of "search" by "replace" in the string "mystring".
	 */
	static string stringReplace(string myString, string search, string replace);
	
private:
	itemMap();
	
};

#endif /*ITEMMAP_H_*/
