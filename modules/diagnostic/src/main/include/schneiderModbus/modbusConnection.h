#ifndef MODBUSCONNECTION_H_
#define MODBUSCONNECTION_H_

#define	PORT_MODBUS		502
//#define MDB_DEBUG

#include <map>
#include <stdlib.h>
#include <stdio.h>
#include "schneiderModbus/item.h"

/** 
 * Manages the network connection to a specific PLC.
 */
class modbusConnection
{
	
public:
	//modbusConnection(string IP);
	/**
	 * Constructor. In order to be able to connect(), you must set a valid
	 * IP using setIp().
	 */
	modbusConnection();
	
	/**
	 * Virtual destructor. Invokes close() to close the socket.
	 */
	virtual ~modbusConnection();
	
	/**
	 * Establish a connection to the plc with the given IP address.
	 */
	void connect(); 

	/**
	 * Creates a packet based upon the items in the list. A transaction
	 * number has to be set before sending the data to the PLC using
	 * retrieveData().
	 * @param	itemsMap	Items to be retrieved.
	 */
	void buildPacket(map<string, item*>* itemsMap);
	
	/**
	 * Retrieve diagnostic data from plc. Invokes extractData() on success.
	 * @return	0 (OK) or -1 (Error)
	 */
	int retrieveData(); 
	
	/**
	 * Adds an item to the internal list (increments the length counter).
	 */
	//void addItem(item* i);
	
	/**
	 * Removes an item of the internal list (decrements the length counter).
	 */
	//void deleteItem(string name);
	
	/**
	 * Stops the execution of the PLC cycle.
	 */
	void stopPlc();
	
	/**
	 * Starts the execution of the PLC cycle.
	 */
	void startPlc();
	
	/**
	 * Returns a list of all items with their current value
	 * (usually invoked after retrieveData() ).
	 */
	map<string, item*>* getItems(); 
	
	/**
	 * Closes connection to the socket. Is invoked automatically by the 
	 * destructor.
	 */
	void close(); // 
	
	/**
	 * Returns the lower byte of the provided unsigned int.
	 */
	static uchar getLowerByte(uint number);
	
	/**
	 * Returns the higher byte of the provided unsigned int.
	 */
	static uchar getHigherByte(uint number);
	
	/**
	 * -1: not connected. 0: connected.
	 */
	int statusConn; 
	
	/**
	 * Set IP address. Mandatory for retrieving data.
	 */
	void setIp(string IP);
	
	
	/**
	 * Returns the status of a specific card (not power supply!) of PREMIUM. 
	 * For each card we have to send a request to the PLC.
	 * @param	rack	Integer 0 .. 7
	 * @param	slot	Integer 1 .. 15
	 * @return	 > 0 (status: PR_CARD_OK | PR_CARD_NOTOK | 
	 * 			PR_CARD_NOTPRESENT | PR_CARD_NA )
	 */
	int getCardStatus(int rack, int slot);
	
	// Status variables for Premium PLCs:	
	/**
	 * Module status ok, no I/O error.
	 */
	static const int PR_CARD_OK 			= 1;
	
	/**
	 * Module status ok, I/O error.
	 */
	static const int PR_CARD_OK_IOERR		= 5;
	
	/**
	 * Module status not ok and I/O error.
	 */
	static const int PR_CARD_NOTOK 			= 2;
	
	/**
	 * Module status not present. (no I/O error resp. unknown)
	 */
	static const int PR_CARD_NOTPRESENT 	= 3;
	
	/**
	 * Module status: Slot is empty.
	 */
	static const int PR_CARD_NA				= 4;
	
	/**
	 * 5 is used by the plugin for "module OK, I/O error"
	 */
	
	/**
	 * Module status: Connector fault.
	 */
	static const int PR_CARD_CONNECTOR_FAULT = 6;

protected:

	/**
	 * Length of the outgoing data in Bytes (= Modbus header + data).
	 */
	uint lengthOut;
	
	/**
	 * Expected length of the response of the PLC in Bytes (header + data).
	 */
	uint lengthIn;
	
	/**
	 * Size of a normal outgoing header.
	 */
	static const uint lengthHeaderOut = 11;
	
	/**
	 * Size of a normal incoming header.
	 */
	static const uint lengthHeaderIn = 11;
	
	/**
	 * Socket descriptor.
	 */
	int socketId;
	
	int iReconnectionTryNumber;


	/**
	 * Pointer to the data that will be transmitted.
	 */
	uchar* dataOut;
	
	/**
	 * Pointer to the incoming data..
	 */
	uchar* dataIn;
	
	/**
	 * Map of all items. They are referenced by their name.
	 */
	map<string, item*>* items;
	
	/**
	 * IP address of the PLC.
	 */
	string IP;
	
	/**
	 * The last transaction ID.
	 */
	uint transactionId;
	
	/**
	 * Returns a different transaction ID for each packet.
	 */
	uint getTransactionId();

	/**
	 * Extracts the data from the recently fetched packet. This means that the
	 * received values are written to the items in the map.
	 */
	void extractData();
	
	/**
	 * Sends any desired data and writes the response in the provided char array.
	 * Sets statusConn = -1, if we receive 0 Bytes.
	 * 
	 * @param	dataOut		Data to send
	 * @param 	lengthOut	Length of outgoing data
	 * @param	*dataIn		Pointer to some free space where the response will be 
	 * 						written, even if the response is shorter or longer (in this
	 * 						case the data will be cut after expectedLength Bytes). 
	 * @param	expectedLength
	 * @return	0 (OK) or -1 (Error)
	 * 
	 */
	int sendData(const uchar* dataOut, int lengthOut, uchar* dataIn, int expectedLength);
	
	/**
	 * Creates socket (without connecting) and sets socket options (timeouts).
	 */
	void createSocket();
private:
    void releaseSocket();
    bool failedToConnect;
	
};

#endif /*MODBUSCONNECTION_H_*/
