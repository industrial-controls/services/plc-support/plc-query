#ifndef SCHNEIDERDRIVER_H
#define SCHNEIDERDRIVER_H
#include <string>
#include <map>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <vector>

#include "Driver.h"
#include "schneiderModbus/item.h"
#include "schneiderModbus/modbusConnection.h"
using namespace std;


/**
 * Class PlcSchneider
 * 
 */
class SchneiderDriver : public Driver {
/**
 * Public stuff
 */
public:
	int schneiderModel;
	static const int PREMIUM = 1;
	static const int QUANTUM = 2;

    SchneiderDriver();
	~SchneiderDriver();
	int disconnect();
	int connect();
	bool isConnected();
    PlcInfoUnionList* queryPlcInfo();

/**
 * Protected stuff
 */
protected:
	modbusConnection* mbConn;
	map<string, item*>* items;
	int statusModules;
	int hasHwConf;
	string plcCurrentTime;
	void resetAllData();
	void resetData();
	void createInfo();
	void updateCardsStatus();
	void addItem(item* i);
	void setSchneiderModel();
	void initDetailsList();
	

private:

	string inline convertToString(int number);

	/**
	 * Flag to indicate that this PLC has to be redone.
	 */
	int 			redo;

	/**
	 * Flag to indicate that this PLC has to be stopped.
	 */
	int				stopFlag;

	/**
	 * Flag to indicate that this PLC has to be start.
	 */
	int				startFlag;

	string			info;
	string			diag;
	/**
	 * If a PLC is offline, the Plc object shall not try on each
	 * refresh() invocation to reconnect, but only e.g. every 5th time.
	 * This counter counts the invocations of refreshData().
	 */
	int	reconnectCounter;


};
#endif //SCHNEIDERDRIVER_H

