/*
 * PlcInfoUnion.h
 */

#ifndef PLCINFOUNION_H_
#define PLCINFOUNION_H_

#include <assert.h>
#include <stdlib.h>
/**
 * A generic Union to hold PLC information
 */
typedef union PlcInfoUnion {
	int intnumber;
	float floatnumber;
	char* stringval;
} PlcInfoUnion;

enum PlcInfoUnionType{
    PLCINFO_UNKNOWN = 0,
    PLCINFO_INTEGER = 1,
    PLCINFO_FLOAT = 2,
    PLCINFO_STRING = 3
};

typedef struct PlcInfoUnionHolder {
	enum PlcInfoUnionType uniontype;
	char* unionname;
	PlcInfoUnion unionval;
} PlcInfoUnionHolder;

typedef struct PlcInfoUnionList {
	int size;
	PlcInfoUnionHolder* values;
} PlcInfoUnionList;


#endif /* PLCINFOUNION_H_ */
