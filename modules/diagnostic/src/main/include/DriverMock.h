/*
 * DriverMock.h
 *
 *  Created on: Aug 5, 2013
 *      Author: enice
 */

#ifndef DRIVERMOCK_H_
#define DRIVERMOCK_H_

#include "Driver.h"

/**
 * A Mock test driver
 */
class DriverMock: public Driver {
public:
	DriverMock();
	virtual ~DriverMock();
	int connect();
	int disconnect();
	PlcInfoUnionList* queryPlcInfo();
	bool isConnected();
};

#endif /* DRIVERMOCK_H_ */
