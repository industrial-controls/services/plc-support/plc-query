/*
 * driver-library.h
 *
 *  Created on: Aug 7, 2013
 *      Author: enice
 */

#ifndef DRIVER_LIBRARY_H_
#define DRIVER_LIBRARY_H_
#include "Driver.h"
#include "PlcInfoUnion.h"
#include "PlcInstance.h"
#include "DriverMock.h"
#include "PlcLogger.h"
#include <string.h>

//#include "siemensSoftnet/SoftnetDriver.h"
#include "schneiderModbus/SchneiderDriver.h"
#include "siemensNoDave/NoDaveDriver.h"

extern "C" void initPlcLibrary(const char* logConfigLocation, int loggingConfigurationWatchInterval);

extern "C" PlcInstance* createPlcInstance(int driverType, const char* ipAddress, const char* hostname, const char* category, const char* plogicalName);

extern "C" int connectPlcInstance(PlcInstance* plcInstance);

extern "C" int disconnectPlcInstance(PlcInstance* plcInstance);
extern "C" struct PlcInfoUnionList* queryPlcInstance(PlcInstance* plcInstance);
extern "C" bool isPlcInstanceConnected(PlcInstance* plcInstance);

extern "C" void cleanupPlcInfoUnionList(PlcInfoUnionList* list);
#endif /* DRIVER_LIBRARY_H_ */
