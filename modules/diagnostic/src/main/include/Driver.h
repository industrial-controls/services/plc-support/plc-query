/*
 * driver.h
 *
 *  Created on: Aug 5, 2013
 *      Author: enice
 */

#ifndef DRIVER_H_
#define DRIVER_H_

#include <string>
#include "PlcInfoUnion.h"

using namespace std;


enum PlcDriverType{
	DRIVER_TYPE_MOCK = 0x01
	,DRIVER_TYPE_STEP7 = 0x02
	,DRIVER_TYPE_MODBUS = 0x04
	,DRIVER_TYPE_NODAVE = 0x08
};

/**
 * Abstract driver definition - A driver is normally stateless and exposes static methods
 * to connect to a given make of PLC.
 */
class Driver {

public:
	Driver ();
	virtual ~Driver () =0;
	virtual int connect () = 0;
	virtual int disconnect() = 0;
	string 		getHostname();
	void 		setHostname(string hostname);
	string 		getIpAddress ();
	void		setIpAddress(string ipAddress);

	virtual bool   isConnected() = 0;

	virtual struct PlcInfoUnionList* queryPlcInfo() = 0;

	static void addStringPropertyToList(PlcInfoUnionList* list, const string& propertyName, const string& propertyValue);
	static void addFloatPropertyToList(PlcInfoUnionList* list, const string& propertyName, float propertyValue);
	static void addIntPropertyToList(PlcInfoUnionList* list, const string& propertyName, int propertyValue);

	static PlcInfoUnionList* initPlcInfoUnionList(int size);

	string getCategory();
	void   setCategory(const char* category);
protected:
	string 			IP;
	string 			hostname;
	string			category;
	int 			reachable;
	int 			connected;

};

#endif /* DRIVER_H_ */
