/*
 * PlcLogger.h
 *
 *  Created on: Aug 7, 2013
 *      Author: enice
 */

#ifndef PLCLOGGER_H_
#define PLCLOGGER_H_
#include "log4cplus/logger.h"
#include "log4cplus/loglevel.h"
#include "log4cplus/loggingmacros.h"
#include "log4cplus/configurator.h"
#include "PlcInstance.h"
#include <string.h>
#include <iostream>
#include <fstream>

using namespace std;
using namespace log4cplus;

/**
 * Utility class to query Log4cplus logger names and instances.
 */
class PlcLogger {
public:
	PlcLogger();
	virtual ~PlcLogger();

	static Logger getLoggerFromPlcInstance(PlcInstance* plcInstance){
		std::string result("plc");

//		assert(strlen(plcInstance->getCategory()) > 0);
		result.append(".");
		result.append(plcInstance->getCategory());
		result.append(".");
//		assert(strlen(plcInstance->getLogicalName()) > 0);
		result.append(plcInstance->getLogicalName());

		return Logger::getInstance(LOG4CPLUS_TEXT(result));
		//return Logger::getRoot();
	}
	static Logger getLoggerFromDriverDetails(Driver* driver){
		std::string result("driver");

//		assert(driver->getCategory().length() > 0);
		result.append(".");
		result.append(driver->getCategory());
		result.append(".");
//		assert(driver->getHostname().length() > 0);
		result.append(driver->getHostname());

		return Logger::getInstance(LOG4CPLUS_TEXT(result));
//		return Logger::getRoot();
	}
	static ConfigureAndWatchThread* initLogging(string logConfigLocation, int loggingConfigurationWatchInterval){
		    Logger root = Logger::getRoot();
			ConfigureAndWatchThread* configureThread = NULL;

		    try {
				if (loggingConfigurationWatchInterval == -1){
					std::ifstream fi(logConfigLocation.c_str());
					if(fi.good()){
					  fi.close();
					  log4cplus::PropertyConfigurator::doConfigure(LOG4CPLUS_TEXT(logConfigLocation));
					  LOG4CPLUS_INFO(root,LOG4CPLUS_TEXT("Configured Log4cplus from properties file"));
					}else{
					  cout << LOG4CPLUS_TEXT("For Information : PLC Library Logging not configured, cannot access '"<<logConfigLocation<<"'.") << endl;
					}
				}else{
					if (loggingConfigurationWatchInterval < 500){
					  cout << LOG4CPLUS_TEXT("Logging configuration watching interval cannot be less than 500 ms."<<endl<<"Now stopping the program.") << endl;
					  exit(-1);
					}else{
						std::ifstream fi(logConfigLocation.c_str());
						if(fi.good()){
							fi.close();
							configureThread = new ConfigureAndWatchThread(
								LOG4CPLUS_TEXT(logConfigLocation), loggingConfigurationWatchInterval);
							LOG4CPLUS_INFO(root,LOG4CPLUS_TEXT("Configured Log4cplus config watch interval to "<<loggingConfigurationWatchInterval<<" ms."));
						}else{
						  cout << LOG4CPLUS_TEXT("For Information : PLC Library Logging not configured, cannot access '"<<logConfigLocation<<"'.") << endl;
						}
					}
				}
			}
			catch(...) {
				cout << LOG4CPLUS_TEXT("Exception while configuring log4cplus...") << endl;
			    LOG4CPLUS_FATAL(root,LOG4CPLUS_TEXT("Exception while configuring log4cplus..."));
			}
			return configureThread;

	}

};

#endif /* PLCLOGGER_H_ */
