/*
 * PlcInstance.h
 *
 *  Created on: Aug 6, 2013
 *      Author: enice
 */

#ifndef PLCINSTANCE_H_
#define PLCINSTANCE_H_

#include "Driver.h"

/**
 * A stateful PLC Instance - which carries the PLC connection parameters, logical name and category
 * for better tracking of the corresponding connection.
 */
class PlcInstance {
public:
	PlcInstance();
	virtual ~PlcInstance();

	Driver* 	getDriver();
	void 		setDriver(Driver* driver);

	int 		connect ();
	int 		disconnect();
	bool		isConnected();
	string  		getHostname();
	void		setHostName(string hostName);
	string 		getIpAddress ();
	void		setIpAddress(string ipAddress);
	string  		getCategory();
	void		setCategory(string  category);
	string  		getLogicalName();
	void		setLogicalName(string  logicalName);

	struct PlcInfoUnionList* queryInfo();
protected:
	Driver* 		driver;
	string 			IP;
	string  			hostname;
	string  			category;
	string  			logicalName;
	int 			reachable;
	int 			connected;
};

#endif /* PLCINSTANCE_H_ */
