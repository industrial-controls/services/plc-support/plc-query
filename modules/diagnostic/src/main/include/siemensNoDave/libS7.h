/*
	LibS7 : library for PLC Siemens diagnostic
*/


#ifdef __cplusplus

extern "C" {
#endif


#ifndef LIBS7_H
#define LIBS7_H
				 
#include <stdlib.h>
#include <stdio.h>
#include "nodave.h"
#include "openSocket.h"

// List of errors
#define S7_SYS_ERROR			(int)-1
#define S7_PARAM_ERROR			(int)-2
#define S7_CONNECT_ERROR		(int)-3
#define S7_DISCONNECT_ERROR		(int)-4
#define S7_TIMEOUT_ERROR		(int)-5
#define S7_EPIPE_ERROR			(int)-6
#define S7_FRAME_ERROR			(int)-7

// Types of PLC
#define SIEMENS_S300 			(int) 3
#define SIEMENS_S400			(int) 4

// All IDRequest for s7GetDiagnostic
#define PLCRunMode 			0x0001
#define PLCIOErr			0x0002
#define PLCErr				0x0004
#define PLCTime				0x0008
#define PLCCycleTime		0x0010
#define PLCLastStartDate	0x0020
#define PLCDiag				0x0040
#define PLCModID			0x0080
#define PLCHardware			0x0100
#define PLCFirmware			0x0200
#define PLC_GENERIC			0x0007
#define PLC_DETAIL			0x0078
#define PLC_ALL				0xFFFF

// Structures
typedef struct{
	daveConnection* pConnexion;
	unsigned char iTypePLC;
}s7Connexion;

typedef struct{
	unsigned char iYear;
	unsigned char iMonth;
	unsigned char iDay;
	unsigned char iHour;
	unsigned char iMinute;
	unsigned char iSecond;
	unsigned short int iMillisecond;
}TDateAndTime;

typedef struct{
	TDateAndTime DateAndTime;
	unsigned short int iEventID;
}TDiagEvent;

typedef struct{
	unsigned short int iID;
	unsigned short int iTypeData;
}THeader;

int s7connect (s7Connexion **pps7Connexion,const char *strIP, int iSlotCPU, long iSecondTimeOut);
int s7Disconnect (s7Connexion *ps7Connexion);
int s7RunPLC (s7Connexion *ps7Connexion);
int s7StopPLC (s7Connexion *ps7Connexion);
int s7Read (s7Connexion *ps7Connexion, 
				unsigned char orgId, 
				unsigned char dbNumber,
				unsigned short start,
				unsigned short size, 
				unsigned char* pData, 
				int iSecondTimeout);
int s7Write (s7Connexion *ps7Connexion, 
				unsigned char orgId,
				unsigned char dbNumber,
				unsigned short start,
				unsigned short size,
				unsigned char* pData,
				int iSecondTimeout);
int s7GetDiagnostic (const s7Connexion *ps7Connexion, unsigned short int iIDRequest,unsigned char* pBuffer, int iBufferLength);
int s7GetStatusLed (const s7Connexion *ps7Connexion, unsigned short int iIDRequest,unsigned char* pBuffer, int iBufferLength);
int s7GetDiagnosticBuffer (const s7Connexion *ps7Connexion, unsigned short int iIDRequest,unsigned char* pBuffer, int iBufferLength, int numItems);
//char* s7GetTextID (int iIdNumber, const char* strFile, int* size,  char* strIdentifiant);
char* s7GetTextID (unsigned short int iIdNumber, const char* strFile, int* size);

#endif //LIBS7_H

#ifdef __cplusplus
//#ifdef CPLUSPLUS
 }
#endif
