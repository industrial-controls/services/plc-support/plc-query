/*
 * NoDaveDriver.h
 *
 *  Created on: Sep 16, 2014
 *      Author: enice
 */

#ifndef NODAVEDRIVER_H_
#define NODAVEDRIVER_H_

#include "Driver.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <sstream>

using namespace std;

class NoDaveDriver: public Driver {
public:
	NoDaveDriver(string ip_addr, string plc_name);
	~NoDaveDriver();
	int connect();
	int disconnect();
	bool isConnected();
	struct PlcInfoUnionList* queryPlcInfo();
	static const int MODULE_FAULTY = 1;
	static const int MODULE_PRESENT = 2;
	static const int MODULE_UNAVAILABLE = 3;
	static const int MODULE_DISABLED = 4;

protected:
	string IP;
	void* pConnexion;
	int S7type;
	int slot;
	int statusHwConf;
	string plcDiagEvent;
	string sPlcCycleTime;
	string sPlcTime;
	string sPlcLastStartDate;
	string sReference;
	string sHardware;
	string sFirmware;
	string diag;
	string info;

private:
	int connectNoDave(string ipAddress);
	inline string convertToString(int number);
	void createInfo();

	void resetData() {
		plcDiagEvent = "";
	}
};
#endif /* NODAVEDRIVER_H_ */
