import requests
import logging
from bs4 import BeautifulSoup
from .constants import *


class Web_Scraper():
    def __init__(self, target_host):
        self.target_host = target_host
        self.base_url = "http://{}".format(self.target_host)
    
    def query_metrics(self):
        results = {}
        try:
            html_text = requests.get(self.base_url+"/Portal/Portal.mwsl?PriNav=Start").text
            start_page = BeautifulSoup(html_text, 'html.parser')
            results[SI_CONN] = 1

            operating_mode = start_page.select("#startpage_status_id").string.strip()
            results.put(SI_MODE_RUN.name(), int(operating_mode== "OK"))

            # Element dateTimeHeader = startPageDocument.select("div#DateTimeContainer").first();
            # results.put(SI_TIME.name(), dateTimeHeader.select("span#date").text() + " " + dateTimeHeader.select("span#time").text());
            
            # Document diagnosticBufferDocument = Jsoup.connect("http://"+host+"/ClientArea/DiagTable.mwsl?Start=1&End=10").get();
            # Element contentTable = diagnosticBufferDocument.select("table.ContentTable").first();
            # StringBuilder diagnosticBufferBuilder = new StringBuilder();
            # contentTable.select("tr.table_row").forEach( rowElement -> {
            #     diagnosticBufferBuilder.append(rowElement.select("td:eq(3)").text()).append(" "); // date
            #     diagnosticBufferBuilder.append(rowElement.select("td:eq(2)").text()); // time
            #     diagnosticBufferBuilder.append(" : ").append(rowElement.select("td:eq(5)").text()); // event
            #     diagnosticBufferBuilder.append("\n");
            # });
            # results.put(SI_DIAG.name(), diagnosticBufferBuilder.toString());
            
            # // Now query the INFO buffer, collecting various pieces of unstructured information
            # try {
            #     StringBuilder infoBufferBuilder = new StringBuilder();
                
            #     /////////////////
            #     // Program protection
            #     Document programProtectionDoc = Jsoup.connect("http://"+host+"/Portal/Portal.mwsl?PriNav=Online&SecNav=ProgramProtection").get();
            #     infoBufferBuilder.append("Program Protection\n------------------\n");
            #     infoBufferBuilder.append("Know-how protection:       "+programProtectionDoc.select("#programprotection_knowhowprotection_value").text()).append("\n");
            #     infoBufferBuilder.append("Memory card serial number: "+programProtectionDoc.select("#programprotection_memorycardserialnumber_value").text()).append("\n");
            #     infoBufferBuilder.append("CPU serial number:         "+programProtectionDoc.select("#programprotection_cpuserialnumber_value").text()).append("\n");
            #     infoBufferBuilder.append("\n");
            #     //
            # //////////////////
            
            # /////////////////
            # // Memory Usage
            # Document memoryDocument = Jsoup.connect("http://"+host+"/Portal/Portal.mwsl?PriNav=Online&SecNav=Memory").get();
            # Element memoryUsage = memoryDocument.select("div#MemoryUsage").first();
            # infoBufferBuilder.append("Memory Usage\n------------\n");
            # memoryUsage.select("div.memBar").forEach( memBarElement -> {
            #     infoBufferBuilder.append(memBarElement.select("div.barDescTop").text()).append(" : ");
            #     infoBufferBuilder.append(memBarElement.select("div.barDesc").text()).append(" ");
            #     infoBufferBuilder.append(memBarElement.select("span.barDescBot").text());
            #     infoBufferBuilder.append("\n");
            # });


        except Exception:
            logging.exception("Could not query PLC host {}".format(self.target_host))
