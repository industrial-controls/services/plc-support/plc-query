from setuptools import setup, find_packages

setup(
      install_requires=['distribute', 'requests>=2.22.0','beautifulsoup==4.9.3'],
      name = 'plc-query-web-scraping',
      python_requires= '>=3.4.0',
      description = 'Web Scraping for PLC Diagnostics queries (SIEMENS S7-1500)',
      author = 'Brice Copy',
      author_email = 'brice.copy@cern.ch',
      url = 'https://gitlab.cern.ch/industrial-controls/services/plc-support/plc-query',
      keywords = ['plc','siemens','web scraping','beautifulsoup'],
      version = '4.0',
      packages =  find_packages('src/main/python')
)
