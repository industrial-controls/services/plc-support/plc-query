/*
 * queryMain.c
 *
 *  Created on: Dec 15, 2014
 *      Author: enice
 */

#include "driver-library.h"
#include "PlcInstance.h"
#include <stdlib.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <iostream>

#include <signal.h>

using namespace rapidjson;
using namespace std;

const char* getPrefixForPlcType(int plcType) {
	switch (plcType) {
	case DRIVER_TYPE_STEP7:
	case DRIVER_TYPE_NODAVE:
		return "SI_";
	case DRIVER_TYPE_MODBUS:
		return "SC_";
	case DRIVER_TYPE_MOCK:
		return "MCK_";
	default:
		return "";
	}
}

int main(int argc, char **argv) {

	if (argc < 5) {
		cout
				<< "DESCRIPTION : plcquery queries a PLC instance to fetch diagnostic data and produces output as JSON."
				<< endl;
		cout
				<< "USAGE : plcquery <DRIVER_TYPE> <IP ADDRESS> <HOSTNAME> <PLC NAME> "
				<< endl;
		cout << "        where driver type is : " << endl;
		cout << "             SIEMENS Softnet = " << DRIVER_TYPE_STEP7 << endl;
		cout << "            Schneider Modbus = " << DRIVER_TYPE_MODBUS << endl;
		cout << "            Local Simulation = " << DRIVER_TYPE_MOCK << endl
				<< endl;
		cout << "             SIEMENS NoDave = " << DRIVER_TYPE_NODAVE << endl;
		cout << "       e.g. for SIEMENS Softnet : " << endl << "    plcquery "
				<< DRIVER_TYPE_STEP7
				<< " 137.138.25.72 PLCCOIS20.cern.ch PLCCOIS20" << endl << endl;
		cout << "       e.g. for Schneider Modbus : " << endl
				<< "    plcquery " << DRIVER_TYPE_MODBUS
				<< " 137.138.25.92 CFP-864-PU2.cern.ch CFP-864-PU2" << endl
				<< endl;
		cout << "       e.g. for SIEMENS NoDave : " << endl << "    plcquery "
				<< DRIVER_TYPE_NODAVE
				<< " 137.138.25.92 CFP-864-PU2.cern.ch CFP-864-PU2" << endl
				<< endl;
		exit(-1);
	}

	initPlcLibrary("plcquery-log4cplus.properties", -1);

	int driverType = atoi(argv[1]);

	PlcInstance* pointer = createPlcInstance(driverType, argv[2], argv[3],
			"query", argv[4]);

	PlcInfoUnionList* result;

	StringBuffer resultBuffer;
	Writer<StringBuffer> jsonWriter(resultBuffer);
	bool success = false;

	jsonWriter.StartObject();
	string connectionMetricName = getPrefixForPlcType(driverType);
	connectionMetricName += "CONN";

	string replyMetricName = getPrefixForPlcType(driverType);
	replyMetricName += "REPLY";

	if (connectPlcInstance(pointer) > 0) {
		jsonWriter.String(connectionMetricName.c_str());
		jsonWriter.Int(1);
		result = queryPlcInstance(pointer);

		for (int i = 0; i < result->size; ++i) {
			jsonWriter.String(result->values[i].unionname);
			if (result->values[i].uniontype == PLCINFO_INTEGER) {
				jsonWriter.Int(result->values[i].unionval.intnumber);
			} else if (result->values[i].uniontype == PLCINFO_FLOAT) {
				jsonWriter.Double(result->values[i].unionval.floatnumber);
			} else if (result->values[i].uniontype == PLCINFO_STRING) {
				jsonWriter.String(result->values[i].unionval.stringval);
			} else {
				jsonWriter.String("UNKNOWN TYPE");
			}
		}

		// Write the reply status (have we received a meaningful answer from the PLC or not)
		{
			jsonWriter.String(replyMetricName.c_str());
			jsonWriter.Int(result->size > 0 ? 1 : 0);
		}

		cleanupPlcInfoUnionList(result);
		disconnectPlcInstance(pointer);
		success = true;
	} else {
		jsonWriter.String(connectionMetricName.c_str());
		jsonWriter.Int(0);
		jsonWriter.String(replyMetricName.c_str());
		jsonWriter.Int(0);
	}

	jsonWriter.EndObject();
	cout << resultBuffer.GetString() << endl;
	return (success ? 0 : 1);
}
